<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConfigurationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurations', function (Blueprint $table) {
            $table->increments('id');
            $table->double('payout_duration')->default(0); 
            $table->double('investor_ref_bonus')->default(0); 
            $table->double('marketer_ref_bonus')->default(0); 
            $table->enum('email_verify', array('Enabled', 'Disabled'))->default('Enabled'); 
            $table->enum('sms_verify', array('Enabled', 'Disabled'))->default('Enabled'); 
            $table->enum('sms_note', array('Enabled', 'Disabled'))->default('Enabled'); 
            $table->string('sms_gateway_url')->nullable(); 
            $table->text('pay_instruction')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configuration');
    }
}
