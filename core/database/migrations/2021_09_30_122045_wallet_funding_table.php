<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WalletFundingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet_fundings', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('role', array('Credit', 'Debit'))->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->double('amount')->default(0);
            $table->string('pop')->nullable();
            $table->string('payment_method')->nullable();
            $table->dateTime('date_approved')->nullable();
            $table->enum('status', array('Pending', 'Approved', 'Rejected'))->default('Pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallet_fundings');
    }
}
