<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InvestmentPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('title')->nullable();
            $table->double('amount')->default(0);
            $table->double('quarter_pay')->default(0);
            $table->double('slot')->nullable();
            $table->double('invested_slot')->nullable();
            $table->string('duration')->nullable();
            $table->text('inv_banner')->nullable();
            $table->integer('roi')->nullable();
            $table->enum('status', array('NowSelling', 'OpeningSoon', 'SoldOut', 'Pending'))->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_plans');
    }
}
