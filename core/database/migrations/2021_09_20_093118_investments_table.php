<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InvestmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->text('plan_detail')->nullable();
            $table->double('duration')->default(0);
            $table->double('amount')->default(0);
            $table->double('slot')->default(0);
            $table->double('total_paid')->default(0);
            $table->double('roi_perc')->default(0);
            $table->double('roi')->default(0);
            $table->double('total_earn')->default(0);
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->enum('status', array('Pending', 'Active', 'Completed'))->default('Pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investments');
    }
}
