<?php

namespace App\Classes;

use App\Models\cryptoPayment;
use App\Models\Currency;
use App\Models\PaymentMethod;

use Illuminate\Http\Request;

class CoinRemitter
{

	/**
	 * Build success response
	 * @param string/array $data
	 * @param int  $code
	 * @return Illuminate\Http\JsonResponse
	*/
    
    

    public static function createInvoice($userName, $amount, $ref, $payCoin){
        $payMeth = PaymentMethod::where('name', $payCoin)->first();

        $successUrl = Config::host().'/backend/laravel/coinremitter_success/'.$ref;
        $failedUrl = Config::host().'/backend/laravel/coinremitter_failed/'.$ref;
        $defCur = Currency::getDefault();

        $url = "https://coinremitter.com/api/v3/".strtoupper($payMeth->name_url)."/create-invoice";
        $fields = [
            'api_key' => $payMeth->api_key,
            'password' => $payMeth->password,
            'address' => $payMeth->address,
            'amount' => $amount,
            'name' => $userName,
            'currency' => $defCur->code,
            'expire_time' => Config::cryptoPaymentTime(),
            // 'notify_url' => 'http://yourdomain.com/notify-url',
            'success_url' => $successUrl,
            'fail_url' => $failedUrl,
            'description' => 'A '.$amount.' '.$defCur->code.' invoice payment rate of '.$payCoin.' has been initiated by '.$userName.' Please make payment to the wallet address below',
            // 'custom_data1' => 'custom_data1',
            // 'custom_data2' => 'custom_data2' 
        ];
        
        //open connection
        $ch = curl_init();
        
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "content-type: multipart/form-data"
        ));
        
        //So that curl_exec returns the contents of the cURL; rather than echoing it
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 
        
        //execute post
        $result = curl_exec($ch);

        $response = json_decode($result, true);
        $response['ref_no'] = $ref;
        return $response;

    }
    
    
    public static function getInvoice($refNo){
        $localInv = cryptoPayment::singleByRef($refNo);
        $payMeth = PaymentMethod::where('name', $localInv->payment_method)->first();

        $url = "https://coinremitter.com/api/v3/".strtoupper($payMeth->name_url)."/get-invoice";
        $fields = [
            'api_key' => $payMeth->api_key,
            'password' => $payMeth->password,
            'invoice_id' => $localInv->inv_id
        ];
        
        //open connection
        $ch = curl_init();
        
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "content-type: multipart/form-data"
        ));
        
        //So that curl_exec returns the contents of the cURL; rather than echoing it
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 
        
        //execute post
        $result = curl_exec($ch);

        $response = json_decode($result, true);
        return $response;

    }



    



//   {
//     "data": {
//         "flag": 1,
//         "msg": "Invoice is successfully created.",
//         "action": "create-invoice",
//         "data": {
//             "id": "6228b3bc40e1c676711e2fc3",
//             "invoice_id": "BTC001",
//             "merchant_id": "6228ac74ac050c10903f70b8",
//             "url": "https://coinremitter.com/invoice/6228b3bc40e1c676711e2fc3",
//             "total_amount": {
//                 "BTC": "0.00011955",
//                 "USD": "5.00000000"
//             },
//             "paid_amount": [],
//             "usd_amount": "5.00000000",
//             "conversion_rate": {
//                 "USD_BTC": "0.00002391",
//                 "BTC_USD": "41822.00000000"
//             },
//             "base_currency": "USD",
//             "coin": "BTC",
//             "name": "random-name",
//             "description": "Hello world",
//             "wallet_name": "MykelBitcoin",
//             "address": "3DHaTS7Akpe6xnJfKx2UpQAY1YaVCUdLb2",
//             "status": "Pending",
//             "status_code": 0,
//             "suceess_url": "http://yourdomain.com/success-url",
//             "fail_url": "http://yourdomain.com/fail-url",
//             "notify_url": "",
//             "expire_on": "2022-03-09 14:13:40",
//             "invoice_date": "2022-03-09 14:03:40",
//             "custom_data1": "custom_data1",
//             "custom_data2": "custom_data2",
//             "last_updated_date": "2022-03-09 14:03:40"
//         }
//     }
// }
   
	

}