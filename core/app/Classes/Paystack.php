<?php

namespace App\Classes;

use Symfony\Component\HttpKernel\Exception\HttpException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Exception;

use Illuminate\Http\Request;

class Paystack
{

	/**
	 * Build success response
	 * @param string/array $data
	 * @param int  $code
	 * @return Illuminate\Http\JsonResponse
	*/
    public static $jsUrl = 'https://js.paystack.co/v1/inline.js';
    public static $verifyUrl = 'https://api.paystack.co/transaction/verify/';
    
    // Test Boss
    public static $key = 'pk_test_72b0fb2bfc1c34e34f94f325d4df4a2dc5c574b2';
    public static $bearer = 'sk_test_0c72d7f1ce05f5497730fe1e508ef063654aebaa';
    

    // Live
    // public static $key = 'pk_live_44d0aaffcf8a1a5cce4de5389ed8d903fc229cdb';
    // public static $bearer = 'sk_live_7b8c2354b52e4553ed200015656e3be42ad49316';

    // TO CHECK FOR ACCOUNT NUMBER
    public static function verifyAccountNumber($accountNumber, $bankCode) {
      $curl = curl_init();  
      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.paystack.co/bank/resolve?account_number=".$accountNumber."&bank_code=".$bankCode,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "Authorization: Bearer ".self::$bearer,
          "Cache-Control: no-cache",
        ),
      ));
      
      $response = curl_exec($curl);
      $err = curl_error($curl);
      
      curl_close($curl);
      
      if ($err) {
        return "cURL Error #:" . $err;
      } else {
        $response = json_decode($response, true);
        $response['bankID'] = $bankCode;
        return $response;
      }
    }


    // CHECK BVN MATCH
    public static function verifyBVN($bvn, $accNum, $bankCode){
      $url = "https://api.paystack.co/bvn/match";
      $fields = [
        'bvn' => "$bvn",
        'account_number' => "$accNum",
        'bank_code' => "$bankCode"
      ];
      
      $fields_string = http_build_query($fields);
      
      //open connection
      $ch = curl_init();
      
      //set the url, number of POST vars, POST data
      curl_setopt($ch,CURLOPT_URL, $url);
      curl_setopt($ch,CURLOPT_POST, true);
      curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Authorization: Bearer ".self::$bearer,
        "Cache-Control: no-cache",
      ));
      
      //So that curl_exec returns the contents of the cURL; rather than echoing it
      curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 
      
      //execute post
      $result = curl_exec($ch);

      $response = json_decode($result, true);
      return $response;
    }


  public static function validate($orderNumber){
    $result = array();
    //The parameter after verify/ is the transaction reference to be verified
    $url = self::$verifyUrl.$orderNumber;
    //echo $gwaySingle['mer_code'];exit();
    $auth = 'Authorization: Bearer '.self::$bearer;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt(
      $ch, CURLOPT_HTTPHEADER, [$auth]
    );

    $request = curl_exec($ch);
    if(curl_error($ch)){
        echo 'error:' . curl_error($ch);
    }

    curl_close($ch);

    if ($request) {
        $result = json_decode($request, true);
    }
        
    return $result;
  }


}