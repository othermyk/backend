<?php

namespace App\Classes;
use App\Classes\CustomDateTime;
use Exception;
use App\Models\User;
use App\Models\Admin;
use App\Classes\Visitor;
use App\Models\Currency;
use App\Models\InvestmentHistory;
use App\Models\Investments;
use App\Models\WebsiteSettings;
use phpinvoice;

class Helper
{

	/**
	 * Build success response
	 * @param string/array $data
	 * @param int  $code
	 * @return Illuminate\Http\JsonResponse
	*/

	
    public static function loginHandler($user=array()) {
    	$payloadArray = array();
        $exp = CustomDateTime::addDate('90 days');
        $payloadArray['id'] = $user['id'];
        $payloadArray['expAlt'] = $exp;
        // if (isset($nbf)) {$payloadArray['nbf'] = strtotime(Settings::currentTime());}
        if (isset($exp)) {$payloadArray['exp'] = strtotime($exp);}

        $token = self::encode($payloadArray, self::serverKey());
        User::where('id', $user['id'])->update([
           'api_token' =>  $token
        ]);
        $user['token'] = $token;
        $user['exp'] = $exp;
        
        return $user;
    }


    public static function adminLoginHandler($admin=array()) {
        Admin::where('id', $admin['id'])->update([
            'last_login' 	=> CustomDateTime::currentTime(),
            'last_login_ip' => Visitor::getIP(),
        ]);
        $payloadArray = array();
        $exp = CustomDateTime::addDate('2 hours');
        $payloadArray['adminID'] = $admin['id'];
        $payloadArray['expAlt'] = $exp;
        if (isset($nbf)) {$payloadArray['nbf'] = strtotime(CustomDateTime::currentTime());}
        if (isset($exp)) {$payloadArray['exp'] = strtotime($exp);}

        $token = self::encode($payloadArray, self::serverKey());
        Admin::where('id', $admin['id'])->update([
            'admin_token' =>  $token
         ]);

        $admin['token'] = $token;
        $admin['exp'] = $exp;

        return $admin;
    }



    public static function serverKey(){
		return '5f2b5cdbe5194f10b3241568fe4e2b24';
	}

	public static function urlsafeB64Encode($input){
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }

    public static function jsonEncode($input){
        $json = json_encode($input);
        if (function_exists('json_last_error') && $errno = json_last_error()) {
            self::handleJsonError($errno);
        } elseif ($json === 'null' && $input !== null) {
            throw new Exception('Null result with non-null input');
        }
        return $json;
    }



    private static function handleJsonError($errno){
        $messages = array(
            JSON_ERROR_DEPTH => 'Maximum stack depth exceeded',
            JSON_ERROR_STATE_MISMATCH => 'Invalid or malformed JSON',
            JSON_ERROR_CTRL_CHAR => 'Unexpected control character found',
            JSON_ERROR_SYNTAX => 'Syntax error, malformed JSON',
            JSON_ERROR_UTF8 => 'Malformed UTF-8 characters' //PHP >= 5.3.3
        );
        throw new Exception(
            isset($messages[$errno])
            ? $messages[$errno]
            : 'Unknown JSON error: ' . $errno
        );
    }


    public static $supported_algs = array(
        'HS256' => array('hash_hmac', 'SHA256'),
        'HS512' => array('hash_hmac', 'SHA512'),
        'HS384' => array('hash_hmac', 'SHA384'),
        'RS256' => array('openssl', 'SHA256'),
        'RS384' => array('openssl', 'SHA384'),
        'RS512' => array('openssl', 'SHA512'),
    );




    public static function sign($msg, $key, $alg = 'HS256'){
        if (empty(static::$supported_algs[$alg])) {
            throw new Exception('Algorithm not supported');
        }
        list($function, $algorithm) = static::$supported_algs[$alg];
        switch($function) {
            case 'hash_hmac':
                return hash_hmac($algorithm, $msg, $key, true);
            case 'openssl':
                $signature = '';
                $success = openssl_sign($msg, $signature, $key, $algorithm);
                if (!$success) {
                    throw new Exception("OpenSSL unable to sign data");
                } else {
                    return $signature;
                }
        }
    }



	public static function encode($payload, $key, $alg = 'HS256', $keyId = null, $head = null)
    {
        $header = array('typ' => 'JWT', 'alg' => $alg);

        if ($keyId !== null) {
            $header['kid'] = $keyId;
        }

        if ( isset($head) && is_array($head) ) {
            $header = array_merge($head, $header);
        }

        $segments = array();
        $segments[] = static::urlsafeB64Encode(static::jsonEncode($header));
        $segments[] = static::urlsafeB64Encode(static::jsonEncode($payload));
        $signing_input = implode('.', $segments);
        $signature = static::sign($signing_input, $key, $alg);
        $segments[] = static::urlsafeB64Encode($signature);

        return implode('', $segments);
        // return implode('.', $segments);
    }


    public static function cronJob(){
        InvestmentHistory::releaseROIFunds();
        Investments::updateCompleteInvestment();
    }




    public static function getInvoice($investID) {
        //Incluse for PDF use
        // include Config::ROOTPATH(). 'app/others/FPDF/phpinvoice.php';

        include base_path(). '/vendor/FPDF/phpinvoice.php';

        
        //Grab single user info
        $settings = WebsiteSettings::single();
        $investment = Investments::findOrFail($investID);
        $investor = User::findOrFail($investment->user_id);
        

        /*echo "<pre>";
        print_r($investor->toArray());
        print_r($investment->toArray());
        exit();*/  
        $invoiceNum = '794344';

        //Items on order
        $orders = array($investment);
        $subtotal = $investment->amount;
        $grandTotal = $investment->amount;
        if ($investment->currency == 'GBP') {
            $currInfo = array('symbol' => '£', 'code' => 'GBP');
        } 
        else if ($investment->currency == 'Euro') {
            $currInfo = array('symbol' => '€', 'code' => 'Euro');
        }
        else {
            $currInfo = array('symbol' => '$', 'code' => 'USD');
        }	    

        //logo URL
        if (@getimagesize($settings->logo_url)) {
            $logoURL = $settings->logo_url;
        } else {
            $logoURL = 'https://mic-phitgloballtd.com/uploads/logo-alt.png';
        }

        //Biz Name
        if(isset($settings->biz_name) AND $settings->biz_name != ''){ 
            $bizName = $settings->biz_name;
        } elseif(isset($settings->site_name) AND $settings->site_name != ''){
            $bizName = $settings->site_name;
        } else { $bizName = 'My Company Name Here'; }

        //Biz Address
        if(isset($settings->biz_addr) AND $settings->biz_addr != ''){ 
            $bizAddr = $settings->biz_addr;
        }else{ $bizAddr = '12, Olaiwaye Street,'; }

        //Biz City
        if(isset($settings->biz_city) AND $settings->biz_city != ''){ 
            $bizCity = $settings->biz_city;
        } else { $bizCity = 'Ikeja'; }

        //Biz State
        if(isset($settings->biz_state) AND $settings->biz_state != ''){ 
            $bizState = $settings->biz_state;
        } else { $bizState = 'Lagos'; }

        //Biz Country
        if(isset($settings->biz_country) AND $settings->biz_country != ''){ 
            $bizCountry = $settings->biz_country;
        } else{ $bizCountry = 'Nigeria'; }

        //invoice status
        if($investment->pay_status == 1){ 
            $invoiceStatus = 'Paid';
            $setColour = "#009966";
        } else {
            $invoiceStatus = 'Unpaid';
            $setColour = "#AA3939";
        }
        
        $invoice = new phpinvoice();
        /* Header Settings */
        $invoice->setLogo($logoURL);
        $invoice->setColor($setColour);
        $invoice->setType("Invoice");
        $invoice->setReference('MPG-'.$investment->id);
        $invoice->setDate(
            $investment->date_paid ? strftime("%B %d, %Y", strtotime($investment->date_paid)) : strftime("%B %d, %Y", strtotime($investment->created_at))
        );
        $invoice->setTime($invoiceStatus);
        $invoice->setDue(0);
        
        //Seller Info
        $invoice->setFrom(
            array($investor->first_name .' '.$investor->last_name, $investor->phone, $investor->email));

        //Buyer Info
        $invoice->setTo(
            array(
                'Investment ID: MPG-'.$investment->id,
                'Payment Method: '.$investment->payment_method, 
                'Deposit Currency: '.$investment->currency,
                $investment->xe_rate ? 'Exchange Rate: '.$investment->xe_rate .' / '.$investment->currency : ''
            )
        );
        //$invoice->flipflop();

        /* Adding Items in table */
        if(!empty($orders)){
            foreach ($orders as $item) {
                $invoice->addItem(
                    'Payment for '. $item->duration.' Investment', 
                    'Invetment Type: '.$investment->type,
                    ($item->pay_status == 1) ? CustomDateTime::dateFrmatAlt($investment->start_date) : '-',
                    ($item->pay_status == 1) ? CustomDateTime::dateFrmatAlt($investment->end_date) : '-',
                    $currInfo['symbol'].number_format($item->amount)
                );
            }
        }
        //calculate vat
        // $vat = (5 * ($subtotal + $shippingCost)) / 100;
        /* Add totals */
        $invoice->addTotal("Subtotal", $currInfo['symbol'].number_format($subtotal));
        // $invoice->addTotal("VAT 5%", $vat);
        // $invoice->addTotal("Shipping", $shippingCost);
        $invoice->addTotal("Grand Total", $currInfo['symbol'].number_format($grandTotal) .' '.$currInfo['code'], true);
        
        /* Set badge */ 
        if($investment->pay_status == 1){
            $invoice->addBadge("Paid Invoice");
        } else {
            $invoice->addBadge("Unpaid Invoice");
        }
        
        
        /* Add title */
        //$invoice->addTitle("Important Notice");
        
        /* Add Paragraph */
        $invoice->addParagraph("NB: This transaction is done on your request based on the MIC-PHIT GLOBAL LIMITED T&C and your preferred investment option.");
        
        /* Set footer note */
        $invoice->setFooternote($bizName);

        //$urlFinal = 'invoice/invoice-'.$invoiceNum.'-'.Settings::randomStrgs(12).'.pdf';
        /* Render */
        $invoice->render('invoice-MPG-'.$investment->id.'.pdf','D'); /* I => Display on browser, D => Force Download, F => local path save, S => return document path */

        return Config::uploadURL().$urlFinal;
    }


    


   
	

}