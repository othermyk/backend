<?php

namespace App\Http\Controllers\CoinPay\Examples;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CoinPay\src\CoinpaymentsAPI;
use Exception;
use App\Traits\ApiResponder;


class ConversionLimit extends Controller
{
    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }



    public function convertLimit(){
        /** Scenario: Show conversion limits between multiple currency pairings.**/

        // Setup pairings in the format "from|to" using currency tickers
        $pairings = [
            'BTC|LTC',
            'BCH|ETH',
            'XRP|XMR', // This pairing does not support conversions and should throw an error.
            'XEM|ZEC'  // This pairing does not support conversions and should throw an error.
        ];

        // Prepare output and empty array for storing errors
        $output = '<table><tbody><tr><td>From</td><td>To</td><td>Minimum</td><td>Maximum</td></tr>';
        $errors = [];

        // Loop through pairings, making API calls for each
        foreach ($pairings as $pairing) {

            // Split the pairing into a from and to value
            $pairing_split = explode('|', $pairing);
            $from = $pairing_split[0];
            $to = $pairing_split[1];

            // Attempt the API call
            try {
                $cps_api = new CoinpaymentsAPI("34157c52A0511a55662e1a2eA326edaeCe4EBC2cfc88E6E253f3F12B34aa7f14", "8b09621defb786b954c2d57896841c758613d5ea1e8994cae00251220d71dcc2", 'json');
                $limit = $cps_api->GetConversionLimits($from, $to);
                $cps_api = null;
            } catch (Exception $e) {
                echo 'Error: ' . $e->getMessage();
                exit();
            }

            // Check for errors and add to output variable or errors array depending on success
            if ($limit['error'] == 'ok') {
                $min = $limit['result']['min'];
                $max = $limit['result']['max'];
                $output .= '<tr><td>' . $from . '</td><td>' . $to . '</td><td>' . $min . '</td><td>' . $max . '</td></tr>';
            } else {
                $errors[] = 'Error for pairing from ' . $from . ' to ' . $to . ' : ' . $limit['error'];
            }
        }

        // Close output HTML, check for errors and echo the output
        $output .= '</tbody></table>';
        if (!empty($errors)) {
            foreach ($errors as $error) {
                $output .= '<br>' . $error;
            }
        }
        echo $output;
    }

}

