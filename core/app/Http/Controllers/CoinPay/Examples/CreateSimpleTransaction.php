<?php

namespace App\Http\Controllers\CoinPay\Examples;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CoinPay\src\CoinpaymentsAPI;
use App\Models\PaymentMethod;
use App\Traits\ApiResponder;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class CreateSimpleTransaction extends Controller
{
    
    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }


    public static function justTest($amount, $curr1, $curr2, $userEmail){

        $method = PaymentMethod::where('name', strtolower($curr2))->first();

        if($method){
            /** Scenario: Create a simple transaction. **/

            // Create a new API wrapper instance
            // $cps_api = new CoinpaymentsAPI($private_key, $public_key, 'json');
            $cps_api = new CoinpaymentsAPI("34157c52A0511a55662e1a2eA326edaeCe4EBC2cfc88E6E253f3F12B34aa7f14", "8b09621defb786b954c2d57896841c758613d5ea1e8994cae00251220d71dcc2", 'json');

            // Enter amount for the transaction
            // $amount = 20000;

            // Litecoin Testnet is a no value currency for testing
            //the country currency
            // $currency1 = 'NGN';

            //cryptocurrency
            // $currency2 = 'BTC';

            // Enter buyer email below
            // $buyer_email = 'uidabujkda@gmil.com';

            // Make call to API to create the transaction
            try {
                $transaction_response = $cps_api->CreateSimpleTransaction(
                    $amount, 
                    $curr1, 
                    strtoupper($method->name_url), 
                    $userEmail
                );
            } catch (Exception $e) {
                echo 'Error: ' . $e->getMessage();
                exit();
            }

            if ($transaction_response['error'] == 'ok') {
                // Success!
                $output = 'Transaction created with ID: ' . $transaction_response['result']['txn_id'] . '<br>';
                $output .= 'Amount for buyer to send: ' . $transaction_response['result']['amount'] . '<br>';
                $output .= 'Address for buyer to send to: ' . $transaction_response['result']['address'] . '<br>';
                $output .= 'Seller can view status here: ' . $transaction_response['result']['status_url'];

            } else {
                // Something went wrong!
                $output = 'Error: ' . $transaction_response['error'];
            }

            // Output the response of the API call
            return json_decode(json_encode($transaction_response));
        } else {
            return null;
        }
    }

}

