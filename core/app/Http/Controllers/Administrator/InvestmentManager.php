<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\InvestmentPlan;
use App\Models\Investments;
use App\Models\InvestmentHistory;
use App\Models\Configuration;
use App\Classes\CustomDateTime;
use App\Models\Wallets;
use App\Models\AboutUs;
use App\Models\Admin;

class InvestmentManager extends Controller
{

    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function investment($token, $limit, $page, $status){
        if(Admin::isAdminAuth($token)){
            if(!$page){
                $page = 1; 
                $offset = 0;
            }
            else{				
                $offset = $limit * ($page - 1);
            }
            $results = array(
                'counts' => Investments::countInv($status), 
                'data' => Investments::getInvForAdmin($limit, $offset, $status)
            );
            return $this->successResponse($results);
        } else {
            return $this->adminAuthError();
        }
    }

    public function investmentByUser($token, $loginID){
        if(Admin::isAdminAuth($token)){
            $records = Investments::eachUserInv($loginID);
            return $this->successResponse($records);
        } else {
            return $this->adminAuthError();
        }
    }
    
    public function plan($token, $limit, $page, $status){
        if(Admin::isAdminAuth($token)){
            if(!$page){
                $page = 1; 
                $offset = 0;
            }
            else{				
                $offset = $limit * ($page - 1);
            }
            $results = array(
                'counts' => InvestmentPlan::countInv($status), 
                'data' => InvestmentPlan::getInvForAdmin($limit, $offset, $status)
            );
            return $this->successResponse($results);
        } else {
            return $this->adminAuthError();
        }
    }



    public function addPlan(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
        
            $result = InvestmentPlan::create($postData);

            return $this->successResponse($result, Response::HTTP_CREATED);     
        } else {
            return $this->adminAuthError();
        }   
    }


    public function singlePlan($token, $id){
        if(Admin::isAdminAuth($token)){
            $record = InvestmentPlan::findOrFail($id);
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }

    public function updatePlan(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $id = $postData['id'];

            $single = InvestmentPlan::findOrFail($id);
            $single->fill($postData);
            $single->save();
        
            return $this->successResponse($single);
        } else {
            return $this->adminAuthError();
        }
    }

    public function deletePlan($token, $id){
        if(Admin::isAdminAuth($token)){
            $record = InvestmentPlan::findOrFail($id);
            $record->delete();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }


    public function approveInvest($token, $investID){
        if(Admin::isAdminAuth($token)){
            $app = Investments::approveInvestment($investID);

            return $this->successResponse($app);
        } else {
            return $this->adminAuthError();
        }
    }


    public function investmentHistory($token, $investID){
        if(Admin::isAdminAuth($token)){
            $record = array(
                'history' => InvestmentHistory::investHistory($investID),
                'investment' => Investments::findOrFail($investID),
            );
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }


    public function deleteInvestment(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();        
            $single = Investments::findOrFail($postData['id']);
            $del = $single->delete();
            if($del){
                if($postData['refund'] == 'yes'){
                    Wallets::createRow(
                        $single->user_id, 
                        $single->amount, 
                        'Credit', 
                        "Refund of the investment deleted on ".CustomDateTime::currentDate(),
                        'Investment Refund'
                    );
                }
                return $this->successResponse($del);
            }
        } else {
            return $this->adminAuthError();
        }
    }


    public function digest($token){
        if(Admin::isAdminAuth($token)){
            $get = AboutUs::where('id', 4)->first();
            return $this->successResponse($get);
        } else {
            return $this->adminAuthError();
        }
    }


    public function updateDigest(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $id = $postData['id'];
            AboutUs::where('id', $id)->update([
                'banner' => $postData['banner'],
                'title' => $postData['title'],
                'content' => $postData['content'],
            ]);
            $result = AboutUs::findOrFail($id);
            return $this->successResponse($result);
        } else {
            return $this->adminAuthError();
        }
    }
    
}
