<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Withdrawal;
use App\Models\Wallets;
use App\Classes\EmailClass;
use App\Classes\CustomDateTime;
use App\Models\Admin;

class WithdrawalManager extends Controller
{
    
    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }



    public function withdrawals($token, $status, $limit=20, $page=1){
        if(Admin::isAdminAuth($token)){
            if(!$page){
                $page = 1; 
                $offset = 0;
            }
            else{				
                $offset = $limit * ($page - 1);
            }
            $results = array(
                'counts' => Withdrawal::allRecordCounts($status), 
                'data' => Withdrawal::allRecords($status, $limit, $offset),
            );
            return $this->successResponse($results);
        } else {
            return $this->adminAuthError();
        }
    }

    public function approveWithdrawal($token, $id){
        if(Admin::isAdminAuth($token)){
            $record = Withdrawal::findOrFail($id);
            if($record->status == 'Pending'){
                $update = Withdrawal::updateWithStatus($id);

                return $this->successResponse($update);
            } else {
                return $this->errorResponse('WIthdrawal has alread approved', Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } else {
            return $this->adminAuthError();
        }
    }

    public function deleteWithdrawal($token, $id){
        if(Admin::isAdminAuth($token)){
            $record = Withdrawal::findOrFail($id);
            $record->delete();
            $delWallet = Wallets::findOrFail($record->wallet_id);
            $delWallet->delete();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }

}
