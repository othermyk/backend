<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\WalletFunding;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Withdrawal;
use App\Models\Admin;

class WalletManager extends Controller
{
    
    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function WalletFund($token, $status, $limit=20, $page=1){
        if(Admin::isAdminAuth($token)){
            if(!$page){
                $page = 1; 
                $offset = 0;
            }
            else{				
                $offset = $limit * ($page - 1);
            }
            $results = array(
                'counts' => WalletFunding::allRecordCounts($status), 
                'data' => WalletFunding::allRecords($status, $limit, $offset),
            );
            return $this->successResponse($results);
        } else {
            return $this->adminAuthError();
        }
    }

    public function approveWalletFund($token, $id){
        if(Admin::isAdminAuth($token)){
            $record = WalletFunding::findOrFail($id);
            if($record->status == 0){
                $update = WalletFunding::approveWalletFunding($id);
                return $this->successResponse($update);
            } else {
                return $this->errorResponse('Fund has alread approved', Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } else {
            return $this->adminAuthError();
        }
    }

    public function deleteWalletFund($token, $id){
        if(Admin::isAdminAuth($token)){
            $record = WalletFunding::findOrFail($id);
            $record->delete();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }

}
