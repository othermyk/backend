<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\AboutUs;
use App\Models\HowItWorks;
use App\Models\AdminBank;
use App\Models\EmailTemplate;
use App\Models\Faq;
use App\Models\HomeBanner;
use App\Models\OurClient;
use App\Models\Admin;

class ContentManager extends Controller
{

    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }


    public function updateAboutUs(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $id = $postData['id'];
            $record = AboutUs::findOrFail($id);
            $record->fill($postData);
            if ($record->isClean()) {
                return $this->successResponse($record);
            }
            $record->save();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }


    public function addHowItWork(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $add = HowItWorks::create($postData);
            return $this->successResponse($add, Response::HTTP_CREATED);
        } else {
            return $this->adminAuthError();
        }
    }

    public function howItWorkList($token){
        if(Admin::isAdminAuth($token)){
            $result = HowItWorks::all();
            return $this->successResponse($result);
        } else {
            return $this->adminAuthError();
        }
    }

    public function howItWorkSingle($token, $id){
        if(Admin::isAdminAuth($token)){
            $result = HowItWorks::findOrFail($id);
            return $this->successResponse($result);
        } else {
            return $this->adminAuthError();
        }
    }

    public function updateHowItWork(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $record = HowItWorks::findOrFail($postData['id']);
            $record->fill($postData);
            if ($record->isClean()) {
                return $this->errorResponse('At least one value must change', Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $record->save();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }


    public function deleteHowItWork($token, $id){
        if(Admin::isAdminAuth($token)){
            $record = HowItWorks::findOrFail($id);
            $record->delete();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }

    public function banks($token){
        if(Admin::isAdminAuth($token)){
            $result = AdminBank::all();
            return $this->successResponse($result);
        } else {
            return $this->adminAuthError();
        }
    }

    public function addBank(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $result = AdminBank::create($postData);
            return $this->successResponse($result, Response::HTTP_CREATED);
        } else {
            return $this->adminAuthError();
        }
    }

    public function bankSingle($token, $id){
        if(Admin::isAdminAuth($token)){
            $result = AdminBank::findOrFail($id);
            return $this->successResponse($result);
        } else {
            return $this->adminAuthError();
        }
    }

    public function updateBank(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $id = $postData['id'];
            $record = AdminBank::findOrFail($id);
            $record->fill($postData);
            if ($record->isClean()) {
                return $this->errorResponse('At least one value must change', Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $record->save();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }


    public function deleteBank($token, $id){
        if(Admin::isAdminAuth($token)){
            $record = AdminBank::findOrFail($id);
            $record->delete();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }

    public function getHomeBanner($token){
        if(Admin::isAdminAuth($token)){
            $record = array(
                'slider' => HomeBanner::first(),
                'aboutUs' => AboutUs::aboutUs(),
            );
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }
    
    public function updateHomeBanner(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $id = $postData['id'];
            $record = HomeBanner::findOrFail($id);
            $record->fill($postData);
            if ($record->isClean()) {
                return $this->successResponse($record);
            }
            $record->save();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }




    // FAQ
    public function faqList($token){
        if(Admin::isAdminAuth($token)){
            $result = Faq::all();
            return $this->successResponse($result);
        } else {
            return $this->adminAuthError();
        }
    }

    public function addFaq(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $add = Faq::create($postData);
            return $this->successResponse($add, Response::HTTP_CREATED);
        } else {
            return $this->adminAuthError();
        }
    }

    public function updateFaq(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $record = Faq::findOrFail($postData['id']);
            $record->fill($postData);
            if ($record->isClean()) {
                return $this->successResponse($record);
            }
            $record->save();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }


    public function deleteFaq($token, $id){
        if(Admin::isAdminAuth($token)){
            $record = Faq::findOrFail($id);
            $record->delete();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }


    // EMAIL TEMPLATE
    public function emailTemplate($token){
        if(Admin::isAdminAuth($token)){
            $record = EmailTemplate::where('status', 1)->get();
            return $this->successResponse($record, Response::HTTP_CREATED);
        } else {
            return $this->adminAuthError();
        }
    }

    public function singleEmailTemplate($token, $id){
        if(Admin::isAdminAuth($token)){
            $record = EmailTemplate::findOrFail($id);
            return $this->successResponse($record, Response::HTTP_CREATED);
        } else {
            return $this->adminAuthError();
        }
    }

    public function updateEmailTemplate(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $record = EmailTemplate::findOrFail($postData['id']);
            $record->fill($postData);
            if ($record->isClean()) {
                return $this->successResponse($record, Response::HTTP_CREATED);
            }
            $record->save();
            return $this->successResponse($record, Response::HTTP_CREATED);
        } else {
            return $this->adminAuthError();
        }
    }


    // CLIENTS
    public function getClient($token){
        if(Admin::isAdminAuth($token)){
            $record = OurClient::all();
            return $this->successResponse($record, Response::HTTP_CREATED);
        } else {
            return $this->adminAuthError();
        }
    }

    public function addClient(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $record = OurClient::create($postData);
            return $this->successResponse($record, Response::HTTP_CREATED);
        } else {
            return $this->adminAuthError();
        }
    }

    public function deleteClient($token, $id){
        if(Admin::isAdminAuth($token)){
            $data = OurClient::findOrFail($id);
            $record = $data->delete();
            return $this->successResponse($record, Response::HTTP_CREATED);
        } else {
            return $this->adminAuthError();
        }
    }

    public function getWhoWeAre($token){
        if(Admin::isAdminAuth($token)){
            $info = array(
                'whoWeAre' => AboutUs::where('id', 2)->first(),
                'callToAction' => AboutUs::where('id', 3)->first()
            );
            return $this->successResponse($info, Response::HTTP_CREATED);
        } else {
            return $this->adminAuthError();
        }
    }

    public function updateWhoWeAre(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $id = $postData['id'];
            $record = AboutUs::findOrFail($id);
            $record->fill($postData);
            if ($record->isClean()) {
                return $this->successResponse($record);
            }
            $record->save();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }

    public function updateCallToAction(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $id = $postData['id'];
            $record = AboutUs::findOrFail($id);
            $record->fill($postData);
            if ($record->isClean()) {
                return $this->successResponse($record);
            }
            $record->save();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }

    
    
}
