<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Guest\Investment;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\WebsiteSettings;
use App\Models\Investments;
use App\Models\User;
use App\Models\Withdrawal;
use App\Models\Vendor;
use App\Models\Admin;
use App\Models\Support;
use App\Models\WalletFunding;

class DashboardInfo extends Controller
{
    
    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function index($token){
        if(Admin::isAdminAuth($token)){
            $content = array(
                'allInvestor' => User::count(),
                'recentSignUp' => User::all()->take(10),
                'blockSuspUsers' => User::blockUser(),
                'allInvestment' => Investments::count(),
                'activeInvestment' => Investments::where('status', 'Active')->count(),
                'pendingInvestment' => Investments::where('status', 'Pendnig')->count(),
                'completeInvestment' => Investments::where('status', 'Completed')->count(),
                'totalWithdrawn' => Withdrawal::where('status', 1)->sum('amount'),
                'totalInvestment' => Investments::where('status', '!=', 'Pending')->sum('amount'),
            
                'recentInv' => Investments::topTenAdmin(),
                'recentUser' => User::topTenAdmin(),

                'clientInfo' => WebsiteSettings::clientInfo(),
                'vendorInfo' => Vendor::getRecord(),

                'walletFundNotice' => WalletFunding::adminNotice(),
                'investNotice' => Investments::adminNotice(),
                'withNotice' => Withdrawal::adminNotice(),
                'unreadSupport' => Support::adminUnread(),
            );
            return $this->successResponse($content);
        } else {
            return $this->adminAuthError();
        }
    }

    
}
