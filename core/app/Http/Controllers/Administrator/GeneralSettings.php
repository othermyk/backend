<?php

namespace App\Http\Controllers\Administrator;

use App\Classes\Settings;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\WebsiteSettings;
use App\Models\Configuration;
use App\Models\SocialSettings;
use App\Models\Currency;
use App\Models\Admin;
use App\Models\BankList;
use App\Models\PaymentMethod;

class GeneralSettings extends Controller
{

    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }


    public function updateWebsiteSettings(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
        
            $id =1;
            $record = WebsiteSettings::findOrFail($id);
            $record->fill($postData);
            // if ($record->isClean()) {
            //     return $this->successResponse($record);
            // }
            $record->save();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }


    public function updateConfiguration(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $id = 1;
            $ref_bonus = ($postData['moreFaqs'][0]['level']) ? json_encode($postData['moreFaqs']): null;

            $postData['ref_bonus'] = $ref_bonus;
            $record = Configuration::findOrFail($id);
            $record->fill($postData);
            if ($record->isClean()) {
                return $this->successResponse($record);
            }
            $record->save();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }

    public function addSocialLink(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all(); 
            $result = SocialSettings::create($postData);
            return $this->successResponse($result, Response::HTTP_CREATED);
        } else {
            return $this->adminAuthError();
        }
    }


    public function deleteSocialLink($token, $id){
        if(Admin::isAdminAuth($token)){
            $record = SocialSettings::findOrFail($id);
            $record->delete();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }

    public function getCurrency($token){
        if(Admin::isAdminAuth($token)){
            $result = Currency::all();
            return $this->successResponse($result);
        } else {
            return $this->adminAuthError();
        }
    }

    public function addCurrency(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all(); 
            $result = Currency::create($postData);
            return $this->successResponse($result, Response::HTTP_CREATED);
        } else {
            return $this->adminAuthError();
        }
    }

    public function setDefaultCurrency($token, $id){
        if(Admin::isAdminAuth($token)){
            $result = Currency::setDefault($id);
            return $this->successResponse($result);
        } else {
            return $this->adminAuthError();
        }
    }

    public function deleteCurrency($token, $id){
        if(Admin::isAdminAuth($token)){
            $record = Currency::findOrFail($id);
            $record->delete();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }


    public function updatePaymentMethod(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $record = PaymentMethod::findOrFail($postData['id']);
            $record->fill($postData);
            if ($record->isClean()) {
                return $this->successResponse($record);
            }
            $record->save();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }
    
    public function getPayMethAdmin($token){
        if(Admin::isAdminAuth($token)){
            $result = PaymentMethod::getAll();
            return $this->successResponse($result);
        } else {
            return $this->adminAuthError();
        }
    }


    

    public function addBankList(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $data = $request->json()->all();
            $create = BankList::create([
                'bank' => $data['bank'],
                'bank_url' => Settings::cleanUrl($data['bank'])
            ]);
            return $this->successResponse($create);
        } else {
            return $this->adminAuthError();
        }
    }


    public function deleteBankList($token, $bankID){
        if(Admin::isAdminAuth($token)){
            $record = BankList::findOrFail($bankID);
            $record->delete();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }
    

    
    
}
