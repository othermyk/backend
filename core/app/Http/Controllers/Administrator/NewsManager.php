<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\News;
use App\Models\Admin;

class NewsManager extends Controller
{
    
    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }



    public function index($token, $limit=20, $page=1){
        if(Admin::isAdminAuth($token)){
            if(!$page){
                $page = 1; 
                $offset = 0;
            }
            else{				
                $offset = $limit * ($page - 1);
            }
            $results = array(
                'counts' => News::count(), 
                'data' => News::take($limit)->offset($offset)->orderBy('id', 'DESC')->get(),
            );
            return $this->successResponse($results);
        } else {
            return $this->adminAuthError();
        }
    }

    public function addNews(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $add = News::create($postData);
            return $this->successResponse($add, Response::HTTP_CREATED);
        } else {
            return $this->adminAuthError();
        }
    }
    
    public function singleNews($token, $id){
        if(Admin::isAdminAuth($token)){
            $record = News::findOrFail($id);
            return $this->successResponse($record, Response::HTTP_CREATED);
        } else {
            return $this->adminAuthError();
        }
    }

    public function updateNews(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $record = News::findOrFail($postData['id']);
            $record->fill($postData);
            if ($record->isClean()) {
                return $this->successResponse($record);
            }
            $record->save();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }


    public function deleteNews($token, $id){
        if(Admin::isAdminAuth($token)){
            $record = News::findOrFail($id);
            $record->delete();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }

    

}
