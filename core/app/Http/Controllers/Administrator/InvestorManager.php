<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserBank;
use App\Models\Wallets;
use App\Models\Withdrawal;
use App\Models\Investments;
use App\Models\Admin;
use App\Models\WalletFunding;
use Illuminate\Http\Response;

class InvestorManager extends Controller
{
    
    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }



    public function investors($token, $role, $limit, $page){
        if(Admin::isAdminAuth($token)){
            if(!$page){
                $page = 1; 
                $offset = 0;
            }
            else{				
                $offset = $limit * ($page - 1);
            }
            $results = array(
                'counts' => User::getUserByCount($role), 
                'data' => User::getUserByStatus($role, $limit, $offset)
            );
            return $this->successResponse($results);
        } else {
            return $this->adminAuthError();
        }
    }

    public function investorDetails($token, $userID){
        if(Admin::isAdminAuth($token)){
            $record = User::findOrfail($userID);
            $data['userInfo'] = $record;        
            $data['bankInfo'] = UserBank::allUserBank($userID);    
            $data['investment'] = Investments::where('user_id', $userID)->orderBy('id', 'DESC')->get();
            return $this->successResponse($data);
        } else {
            return $this->adminAuthError();
        }
    }

    public function updateInvestor(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $record = User::findOrFail($postData['userID']);
            $postData['username'] = $record->username;
            $postData['email'] = $record->email;
            $record->fill($postData);
            if ($record->isClean()) {
                return $this->successResponse($record);
            }
            $record->save();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }

    public function deleteInvestor($token, $user_id){
        if(Admin::isAdminAuth($token)){
            $record = User::findOrFail($user_id);
            $record->delete();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }

    public function updateUserBank(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $postData = $request->json()->all();
            $bankID = $postData['bankID'];
            $record = UserBank::findOrFail($bankID);
            $record->fill($postData);
            if ($record->isClean()) {
                return $this->successResponse($record);
            }
            $record->save();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }
    
    public function deleteBank($token, $bankID){
        if(Admin::isAdminAuth($token)){
            $record = UserBank::findOrFail($bankID);
            $record->delete();
            return $this->successResponse($record);
        } else {
            return $this->adminAuthError();
        }
    }


    public function investorWallet($token, $userID, $limit, $page){
        if(Admin::isAdminAuth($token)){
            if(!$page){
                $page = 1; 
                $offset = 0;
            }
            else{				
                $offset = $limit * ($page - 1);
            }
            $results = array(
                'counts' => Wallets::userWalletAdminCount($userID), 
                'data' => Wallets::userWalletAdmin($userID, $limit, $offset),
                'totalWallet' => Wallets::totalCredit($userID),
                'availableWallet' => Wallets::getBalance($userID)
            );
            return $this->successResponse($results);
        } else {
            return $this->adminAuthError();
        }
    }


    public function investorWalletFunding($token, $userID, $limit, $page){
        if(Admin::isAdminAuth($token)){
            if(!$page){
                $page = 1; 
                $offset = 0;
            }
            else{				
                $offset = $limit * ($page - 1);
            }
            $results = array(
                'counts' => WalletFunding::userWallFundAdminCount($userID), 
                'data' => WalletFunding::userWallFundAdmin($userID, $limit, $offset),
                'totalFund' => WalletFunding::totalCredit($userID),
                'availableFund' => WalletFunding::availableBalance($userID)
            );
            return $this->successResponse($results);
        } else {
            return $this->adminAuthError();
        }
    }
    
    public function investorWithdrawal($token, $userID, $limit, $page){
        if(Admin::isAdminAuth($token)){
            if(!$page){
                $page = 1; 
                $offset = 0;
            }
            else{				
                $offset = $limit * ($page - 1);
            }
            $results = array(
                'counts' => Withdrawal::userCount($userID), 
                'data' => Withdrawal::userWithddrawal($userID, $limit, $offset),
                'availableBalance' => Wallets::getBalance($userID)
            );
            return $this->successResponse($results);
        } else {
            return $this->adminAuthError();
        }
    }
    
    public function investorDownlines($token, $userID, $limit, $page){
        if(Admin::isAdminAuth($token)){
            $user = User::findOrFail($userID);
            if(!$page){
                $page = 1; 
                $offset = 0;
            }
            else{				
                $offset = $limit * ($page - 1);
            }
            $results = array(
                'counts' => User::countAllReferral($user->username), 
                'data' => User::allReferral($user->username, $limit, $offset)
            );
            return $this->successResponse($results);
        } else {
            return $this->adminAuthError();
        }
    }


    public function investorAction($token, $userID, $action){
        if(Admin::isAdminAuth($token)){
            $user = User::findOrFail($userID);
            if(mb_strtolower($action) == 'delete') {
                $result = $user->delete();
                $data = array(
                    'status' => 'success'
                );
                return $this->successResponse($data);
            } else {
                $result = User::accountAction($userID, $action);
                if ($result) {
                    return $this->successResponse(User::findOrFail($userID));
                }
            }
        } else {
            return $this->adminAuthError();
        }
    }


    public function userWalletAction(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $data = $request->json()->all();
            $create = WalletFunding::create([
                'user_id' => $data['userID'], 
                'amount' => $data['amount'], 
                'role' => ucwords($data['status']),
                'payment_method' => ucwords($data['description']),
                'status' => 'Approved',
                'created_at' => $data['date'],
                'updated_at' => $data['date'],
                'date_approved' => $data['date'],
            ]);
            return $this->successResponse($create);
        } else {
            return $this->adminAuthError();
        }
    }


    public function searchInvestor($token, $keyword){
        if(Admin::isAdminAuth($token)){
            $result = array(
                'data' => User::adminSearch($keyword),
                'counts' => User::adminSearchCount($keyword)
            );
            return $this->successResponse($result);
        } else {
            return $this->adminAuthError();
        }
    }
    
    public function verifyUser($token, $userID){
        if(Admin::isAdminAuth($token)){
            $verify = User::where('id', $userID)->update([
                'email_verify' => 1
            ]);
            $user = User::findOrFail($userID);
            return $this->successResponse($user);
        } else {
            return $this->adminAuthError();
        }
    }
    
    public function addNewUser(Request $request, $token){
        if(Admin::isAdminAuth($token)){
            $data = $request->json()->all();
            $checkUsername = User::where('username', $data['username'])->first();
            $checkEmail = User::where('email', $data['email'])->first();
            $checkPhone = User::where('phone', $data['phone'])->first();

            if($checkUsername){
                return $this->errorResponse('Oops! Username already exist', Response::HTTP_UNPROCESSABLE_ENTITY);
            } else if($checkEmail){
                return $this->errorResponse('Oops! email already exist', Response::HTTP_UNPROCESSABLE_ENTITY);
            } else if($checkPhone){
                return $this->errorResponse('Oops! phone number already exist', Response::HTTP_UNPROCESSABLE_ENTITY);
            } else {
                $data['status'] = "Active";
                $data['email_verify'] = 1;
                $data['password'] = app('hash')->make($data['password']);;
                $create = User::create($data);

                return $this->successResponse($create);
            }            
        } else {
            return $this->adminAuthError();
        }
    }
}
