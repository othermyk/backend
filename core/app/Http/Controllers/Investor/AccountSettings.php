<?php

namespace App\Http\Controllers\Investor;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\User;
use App\Models\UserBank;
use App\Models\UserVerification;
use Illuminate\Support\Facades\Auth;
use App\Classes\Helper;
use App\Classes\Settings;
use App\Classes\EmailClass;
use App\Models\UserCrypto;

class AccountSettings extends Controller
{
    
    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        // $this->middleware('auth');
        //To specific the functions you want to allow only auth user to access
        // $this->middleware('auth', ['only' => ['updateProfile', 'index']]);
    }

    public function emailVerification(Request $request){
        $postData = $request->json()->all();
        $user_id = Auth::user()->id;

        $check = UserVerification::verifyEmail($user_id, $postData['email_code']);
        if($check){
            return Helper::loginHandler($check);
        } else {
            return $this->errorResponse('Oops! incorrect token', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
    public function resendToken(){
        $user_id = Auth::user()->id;
        $user = User::findOrFail($user_id);
        $email_code = Settings::randomStrgs(6);
        $resend = UserVerification::createVerification($user_id, $email_code);
        $email_code = Settings::randomStrgs(6);
        UserVerification::createVerification($user->id, $email_code);
        EmailClass::email_verification($user, $email_code);
        if($resend){
            return 'success';
        }
    }

    // update profile
    public function updateProfile(Request $request){
        $user_id = Auth::user()->id;
        $postData = $request->json()->all();
        $record = User::findOrFail($user_id);
        unset($postData['username']);
        unset($postData['email']);
        $record->fill($postData);
        if ($record->isClean()) {
            return $this->errorResponse('At least one value must change', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $record->save();
        return Helper::loginHandler($record);
    }


    public function changePassword(Request $request){
        $user_id = Auth::user()->id;
        $postData = $request->json()->all();
        $record = User::findOrFail($user_id);
        if(!$this->verifyPassword($postData['old_password'], $record->password)){
            return $this->errorResponse('Oops! Old password not matched', Response::HTTP_UNPROCESSABLE_ENTITY);
        } else {
            User::updatePassword($postData['password'], $user_id);
        }
    }

    public function bankInfo(){
        $user_id = Auth::user()->id;
        $apply = UserBank::allUserBank($user_id);
        return $this->successResponse($apply);
    }

    public function addBankInfo(Request $request){
        $user_id = Auth::user()->id;
        $postData = $request->json()->all();
        $postData['user_id'] = $user_id;
        $add = UserBank::create($postData);
        return $this->successResponse($add);
    }

    public function updateBankInfo(Request $request){
        $postData = $request->json()->all();
        $bankID = $postData['id'];
        $update = UserBank::updateBank($postData);
        
        return $this->successResponse($update);
    }

    public function logout(){
        $user_id = Auth::user()->id;
        $record = User::logout($user_id);
        return $this->successResponse($record);
    }



    public function updateCryptoInfo(Request $request){
        $userId = Auth::user()->id;
        $data = $request->json()->all();
        $send = UserCrypto::addOrUpdate($data, $userId);

        return $this->successResponse($send);
    }
    
    public function getUserCrypto(){
        $userId = Auth::user()->id;
        
        $send = UserCrypto::userList($userId);

        $newArray = array();
        foreach($send as $value)
        {
            if(!isset($newArray[$value->crypto]))
            {
                $newArray[$value->crypto] = array();
            }

            $newArray[$value->crypto] = $value->wallet_id;
        }
        $newArray['user_id'] = $userId;
        
        return $this->successResponse($newArray);
    }


    // public function updateCryptoInfo(Request $request){
    //     $userId = Auth::user()->id;
    //     $data = $request->json()->all();
    //     $send = UserCrypto::addOrUpdate($data, $userId);

    //     return $this->successResponse($send);
    // }
    
    // public function getUserCrypto(){
    //     $userId = Auth::user()->id;
        
    //     $send = UserCrypto::singleByUserId($userId);

    //     return $this->successResponse($send);
    // }
}
