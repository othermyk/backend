<?php

namespace App\Http\Controllers\Investor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Guest\Investment;
use App\Models\InvestmentHistory;
use App\Models\Investments;
use App\Models\ReferralBonus;
use App\Models\Support;
use App\Models\WalletFunding;
use App\Traits\ApiResponder;
use App\Models\Wallets;
use App\Models\UserBank;
use Illuminate\Support\Facades\Auth;

class Dashboard extends Controller
{
    
    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function index(){
        $user_id = Auth::user()->id;
        $content = array(
            'availableBalance' => Wallets::getBalance($user_id),
            'walletFundBal' => WalletFunding::availableBalance($user_id),
            'investmentIncome' => InvestmentHistory::totalIncome($user_id),
            'investmentCapital' => Investments::investCapital($user_id),
            'totalFund' => WalletFunding::walletFundingPaid($user_id),
            'availableFund' => WalletFunding::availableBalance($user_id),
            'refBonus' => ReferralBonus::sumRefCom($user_id),
            'fund_history' => Wallets::userWalletFirstTen($user_id),
            'bankInfo' => UserBank::singleByUserId($user_id),
            'unreadSupport' => Support::userUnread($user_id)
        );
        return $this->successResponse($content);
    }


}
