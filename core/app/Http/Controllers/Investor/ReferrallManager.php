<?php

namespace App\Http\Controllers\Investor;

use App\Classes\CustomDateTime;
use App\Classes\EmailClass;
use App\Http\Controllers\Controller;
use App\Models\Configuration;
use App\Models\ReferralBonus;
use App\Models\User;
use App\Traits\ApiResponder;
use Illuminate\Support\Facades\Auth;

class ReferrallManager extends Controller
{
    
    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }



    public function index($limit=20, $page=1){
        if(!$page){
            $page = 1; 
            $offset = 0;
        }
        else{				
            $offset = $limit * ($page - 1);
        }
        $userName = Auth::user()->username;       
        $userID = Auth::user()->id;       
        $downlinesTree = array();
        
        $referral_level = Configuration::referralLevel();
        $downlines = array();
        for ($i=0; $i < $referral_level; $i++) { 
            $downlines = User::userDownliners($userName, $i+1, $limit, $offset);
            // array_push($downlinesTree, $downlines);
            if($downlines['total']){
                array_push($downlinesTree, $downlines);
            } else {
                $resp = $downlinesTree;
                break;
            }
           
        }

        $resp = array();
        if($resp){
            $toRetur = $resp;
        } else {
            $toRetur = $downlinesTree;
        }

        $sumTotal = 0;
        foreach($toRetur as $value){
            $sumTotal += $value['total'] ;
        }

        $results = array(
            'counts' => $sumTotal, 
            'data' => $toRetur,
            'refBonus' => ReferralBonus::sumRefCom($userID),
        );       
        
        return $this->successResponse($results);
    }


}
