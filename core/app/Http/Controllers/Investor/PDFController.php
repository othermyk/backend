<?php

namespace App\Http\Controllers\Investor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Guest\Investment;
use App\Models\InvestmentHistory;
use App\Models\Investments;
use App\Models\Support;
use App\Models\WalletFunding;
use App\Traits\ApiResponder;
use App\Models\Wallets;
use App\Models\UserBank;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use Codedge\Fpdf\Fpdf\Fpdf;

class PDFController extends Controller
{
    
    use ApiResponder;
    
    

    private $fpdf;
 
    public function __construct()
    {
         
    }
 
    public function createPDF()
    {

        $html='
            <h1>Welcome Home</h1>

            <div style="display: flex; justify-content: center">
                <span style="">
                    <img src="https://mic-phitgloballtd.com//template/default/assets/images/small-icon/logo.png">
                </span>
                <span style="width: 400px">
                    <span>INVOICE</span>
                </span>
            </div>
            
            
            <table border="1" style="border:1px solid red">
            <tr>
            <td width="200" height="30">cell 1</td><td width="200" height="30" bgcolor="#D0D0FF">cell 2</td>
            </tr>
            <tr>
            <td width="200" height="30">cell 3</td><td width="200" height="30">cell 4</td>
            </tr>
            </table>
        ';

        $contFill = "Sets the font used to print character strings. It is mandatory to call this method at least once before printing text or the resulting document would not be valid. The font can be either a standard one or a font added via the AddFont() method. Standard fonts use the Windows encoding cp1252 (Western Europe). The method can be called before the first page is created and the font is kept from page to page. If you just wish to change the current font size, it is simpler to call SetFontSize().";

        // return "udhaijk";
        $this->fpdf = new Fpdf;
        $this->fpdf->AddPage();
        $this->fpdf->AcceptPageBreak();
        $this->fpdf->SetFont('helvetica', '', 10);
        $this->fpdf->Image("https://www.ozitechhost.com/images/logo.fw.png");
        // $this->fpdf->WriteHTML($html);

        $this->fpdf->Cell(0,10,"Registration Details",1,1,'C');
        
        $this->fpdf->Cell(80,10,"ORDER INFORMATION",1,0);
        $this->fpdf->Cell(40,10,"START DATE",1,0,'C');
        $this->fpdf->Cell(40,10,"END DATE",1,0,'C');
        $this->fpdf->Cell(30,10,"AMOUNT",1,1,'C');
        
        // $this->fpdf->Cell(0,10,"More information",1,1,'C');

        $this->fpdf->Cell(80,10,"Payment for 1 year Investmen",1,0);
        $this->fpdf->Cell(40,10," Aug 26, 2021",1,0,'C');
        $this->fpdf->Cell(40,10,"Aug 26, 2022",1,0,'C');
        $this->fpdf->Cell(30,10,"$500,000",1,1,'C');

        $this->fpdf->Cell(80,10,"",0,0);
        $this->fpdf->Cell(40,10,"",0,0,'C');
        $this->fpdf->Cell(40,10,"Subtotal",1,0);
        $this->fpdf->Cell(30,10,"$500,000",1,1,'C');

        $this->fpdf->Cell(80,10,"",0,0);
        $this->fpdf->Cell(40,10,"",0,0,'C');
        $this->fpdf->Cell(40,10,"Grand Total",1,0);
        $this->fpdf->Cell(30,10,"$500,000",1,1,'C');
         
        $this->fpdf->Output();
        exit;
    }


}
