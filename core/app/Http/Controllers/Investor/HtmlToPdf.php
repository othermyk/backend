<?php

namespace App\Http\Controllers\Investor;

use App\Classes\CustomDateTime;
use App\Classes\Settings;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\Investments;
use App\Models\User;
use App\Models\WebsiteSettings;
use App\Traits\ApiResponder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Auth;
use Spipu\Html2pdf\Html2pdf;

class HtmlToPdf extends Controller
{
    
    use ApiResponder;
    
    

    private $fpdf;
 
    public function __construct()
    {
         
    }
 
    public function createPDF($investID, $userID)
    {

        
        include base_path(). '/vendor/Spipu/Html2pdf/src/Html2pdf.php';

        $inv = Investments::findOrFail($investID);

        if($inv->user_id == $userID){
            $user = User::findOrFail($inv->user_id);
            $pack = json_decode($inv->plan_detail);
            $defCur = Currency::getDefault();
            $webset = WebsiteSettings::first();
            if($inv->status != 'Pending'){
                $status = 'Paid';
            } else {
                $status = 'Unpaid';
            }

            

            $html='
                <div>
                    <div class="topHeader">
                        <table  style="width:100%; margin-bottom: 50px">
                            <tr>
                                <td style="vertical-align: middle; width: 490px;">
                                    <img src="'.$webset->logo_url.'">
                                </td>
                                <td style="width: 190px;text-align: right;">
                                    <h1 style="margin-bottom: 10px">INVOICE</h1>
                                    <table>
                                        <tr>
                                            <td style="height: 17px; width: 120px; text-align: left; color: #009966; font-weight: bold">INVESTMENT ID:</td>
                                            <td style="height: 17px; width: 100px; text-align: right">XER-'.$investID.'</td>
                                        </tr>
                                        <tr>
                                            <td style="height: 17px; width: 120px; text-align: left; color: #009966; font-weight: bold">DATE:</td>
                                            <td style="height: 17px; width: 100px; text-align: right">'.CustomDateTime::dateFrmatAlt($inv->created_at).'</td>
                                        </tr>
                                        <tr>
                                            <td style="height: 17px; width: 120px; text-align: left; color: #009966; font-weight: bold">STATUS:</td>
                                            <td style="height: 17px; width: 100px; text-align: right">'.$status.'</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table  style="width:100%; margin-bottom: 50px">
                            <tr>
                                <td style="">
                                    <table>
                                        <tr>
                                            <td style="height: 25px; vertical-align:middle; padding-left: 10px; width: 300px; text-align: left; color: #fff; font-weight: bold; background: #009966">BILLING INFORMATION</td>
                                        </tr>
                                        <tr>
                                            <td style="height: 17px; width:; text-align: left;">'.$user->first_name.' '.$user->last_name.'</td>
                                        </tr>
                                        <tr>
                                            <td style="height: 17px; width:; text-align: left; color: #777">'.$user->phone.'</td>
                                        </tr>
                                        <tr>
                                            <td style="height: 17px; width:; text-align: left; color: #777">'.$user->email.'</td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table> 
                                        <tr>
                                            <td style="height: 25px; width: 60px;"></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td style="height: 25px; vertical-align:middle; padding-left: 10px; width: 300px; text-align: left; color: #fff; font-weight: bold; background: #009966">PAYMENT INFORMATION</td>
                                        </tr>
                                        <tr>
                                            <td style="height: 17px; width:; text-align: left;">Investment ID: XER-'.$investID.'</td>
                                        </tr>
                                        <tr>
                                            <td style="height: 17px; width:; text-align: left; color: #777">Investment Plan: '.$pack->name.'</td>
                                        </tr>
                                        <tr>
                                            <td style="height: 17px; width:; text-align: left; color: #777">Payment Method: from wallet</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table  style="width:100%">
                            <tr>
                                <th style="background: #f0f0f0; font-size: 12px; vertical-align: middle; width: 290px; height:45px; padding-left: 7px">ORDER INFORMATION</th>
                                <th style="background: #f0f0f0; font-size: 12px; vertical-align: middle; width: 155px; height:45px; text-align: center">START DATE</th>
                                <th style="background: #f0f0f0; font-size: 12px; vertical-align: middle; width: 155px; height:45px; text-align: center">END DATE</th>
                                <th style="background: #f0f0f0; font-size: 12px; vertical-align: middle; width: 120px; height:45px; text-align: center">AMOUNT</th>
                            </tr>
                            <tr>
                                <td style="background: #f0f0f0; font-size: 12px; vertical-align: middle; height:45px; padding-left: 7px">Payment for '.$pack->title.' investment</td>
                                <td style="background: #f0f0f0; font-size: 12px; vertical-align: middle; height:45px; text-align: center">'.CustomDateTime::dateFrmatAlt($inv->start_date).'</td>
                                <td style="background: #f0f0f0; font-size: 12px; vertical-align: middle; height:45px; text-align: center">'.CustomDateTime::dateFrmatAlt($inv->end_date).'</td>
                                <td style="background: #f0f0f0; font-size: 12px; vertical-align: middle; height:45px; text-align: center">'.Settings::decimalsChcked($inv->amount).'</td>
                            </tr>
                            <tr>
                                <td style="border: none">
                                    
                                </td>
                                <td style="border: none"></td>
                                <td style="background: #f0f0f0; font-size: 12px; vertical-align: middle; height:45px; font-weight: bold;; padding-left: 10px">Slot</td>
                                <td style="background: #f0f0f0; font-size: 12px; vertical-align: middle; height:45px; text-align: center; font-weight: bold;">'.$inv->slot.'</td>
                            </tr>
                            <tr>
                                <td style="border: none"></td>
                                <td style="border: none"></td>
                                <td style="font-size: 13px; vertical-align: middle; height:45px; background:#009966; color:#fff; font-weight: bold; padding-left: 10px">Grand Total</td>
                                <td style="font-size: 13px; vertical-align: middle; height:45px; text-align: center; background:#009966; color:#fff; font-weight: bold">'.$defCur->code.' '.Settings::decimalsChcked($inv->total_paid).'</td>
                            </tr>
                        </table>
                        <p style="margin-top: 60px; font-size: 12px">
                            NB: This transaction is done on your request based on the MIC-PHIT GLOBAL LIMITED T&C and your preferred investment option
                        </p>
                    </div>
                </div>
            ';

            $html2pdf = new Html2Pdf('P', 'A4', 'en');
            $html2pdf->writeHTML($html);

            $destination_path = 'xer-'.$investID.''.Settings::randomStrgs(5).'.pdf';

            $html2pdf->output($destination_path);
        } else {
            return null;
        }

        
    }


}
