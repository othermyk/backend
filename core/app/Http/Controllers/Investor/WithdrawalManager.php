<?php

namespace App\Http\Controllers\Investor;

use App\Classes\CustomDateTime;
use App\Classes\EmailClass;
use App\Http\Controllers\Controller;
use App\Models\Configuration;
use App\Models\User;
use App\Models\UserBank;
use App\Models\UserCrypto;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Withdrawal;
use App\Models\Wallets;
use App\Models\UserNotification;
use Illuminate\Support\Facades\Auth;

class WithdrawalManager extends Controller
{
    
    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }



    public function withdrawals($limit=20, $page=1){
        if(!$page){
            $page = 1; 
            $offset = 0;
        }
        else{				
            $offset = $limit * ($page - 1);
        }
        $user_id = Auth::user()->id;
        $results = array(
            'counts' => Withdrawal::userCount($user_id), 
            'data' => Withdrawal::userWithddrawal($user_id, $limit, $offset),
        );
        return $this->successResponse($results);
    }

    public function submitWithRequest(Request $request){
        $user_id = Auth::user()->id;
        $postData = $request->json()->all();

        $postData['user_id'] = $user_id;
        $amount = $postData['amount'];
        $balance = Wallets::getBalance($user_id);
        $config = Configuration::first();

        if($amount > $balance){
            return $this->errorResponse('Oops! Your wallet balance is not sufficient to withdraw '. number_format($postData['amount']), Response::HTTP_UNPROCESSABLE_ENTITY);
        } else if($amount < $config->min_withdrawal){
            return $this->errorResponse('Oops! sorry the minimum withdrawal request is '. number_format($config->min_withdrawal), Response::HTTP_UNPROCESSABLE_ENTITY);
        } else {
            $create = Wallets::createRow($user_id, $amount, 'Debit', 'Withdrawal request initiated', 'Withdrawal');
            $postData['wallet_id'] = $create->id;

            if($postData['bank_id']){
                $postData['pay_to'] = UserBank::findOrFail($postData['bank_id']);
                $postData['pay_method'] = "bank";
            } else if($postData['crypto']){
                $crypt = strtolower($postData['crypto']);
                $wallId = UserCrypto::singleByCrypto($user_id, $crypt);
                $postData['pay_to'] = '{"cryptocurrency":"'.$crypt.'","wallet_address":"'.$wallId->wallet_id.'"}';
                $postData['pay_method'] = "cryptocurrency";
            }
            
            $post = Withdrawal::create($postData);



            if($post){
                $userInfo = User::findOrFail($user_id);
                UserNotification::createRow(
                    $user_id, 
                    'Withdrawal Request', 
                    'You withdrawal request of '.$amount.' initiated today '.CustomDateTime::currentDate().' has been submitted successfully.'
                );
                EmailClass::withdrawalRequest($userInfo, $amount);
                return $this->successResponse($post);
            }
        }
    }

}
