<?php

namespace App\Http\Controllers\Investor;

use App\Classes\CoinRemitter;
use App\Classes\Config;
use App\Classes\CustomDateTime;
use App\Classes\Paystack;
use App\Classes\Settings;
use App\Http\Controllers\CoinPay\Examples\CreateSimpleTransaction;
use App\Http\Controllers\Controller;
use App\Models\UserBank;
use App\Models\WalletFunding;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Wallets;
use App\Models\AdminBank;
use App\Models\Configuration;
use App\Models\cryptoPayment;
use App\Models\Currency;
use App\Models\UserCrypto;
use App\Models\Withdrawal;
use Illuminate\Support\Facades\Auth;

class WalletManager extends Controller
{
    
    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function allFunds(){
        $user_id = Auth::user()->id;
        $config = Configuration::first();

        $send = UserCrypto::userList($user_id);
        $newArrayCrypt = array();
        foreach($send as $value)
        {
            if(!isset($newArrayCrypt[$value->crypto]))
            {
                $newArrayCrypt[$value->crypto] = array();
            }
            $newArrayCrypt[$value->crypto] = $value->wallet_id;
        }
        $newArrayCrypt['user_id'] = $user_id;


        $content = array(
            'availableBalance' => Wallets::getBalance($user_id),
            'paymentInstruction' => $config->pay_instruction,
            'admin_bank' => AdminBank::getActiveBanks(),
            'bank_info' => UserBank::allUserBank($user_id),
            'walletFundBal' => WalletFunding::availableBalance($user_id),
            'fund_history' => WalletFunding::userWalletFund($user_id),
            'wallet_history' => Wallets::userWallet($user_id),
            'with_history' => Withdrawal::userWith($user_id),
            'userCrypto' => $newArrayCrypt,
            'payStackKey' => Paystack::$key,
            'payStackBearer' => Paystack::$bearer,
            'paystackRef' => Settings::genRefNo('WAL')
        );
        return $this->successResponse($content);
    }

    public function creditWallet(Request $request){
        $userEmail = Auth::user()->email;
        $userName = Auth::user()->username; 
        $userID = Auth::user()->id; 
        $postData = $request->json()->all();
        $postData['user_id'] = Auth::user()->id; 
        $postData['role'] = 'Credit';
        $payMethod = strtolower($postData['payment_method']);

        if(strtolower($postData['payment_method']) == 'paystack'){
            $paymentCheck = Paystack::validate($postData['ref_no']);
            if(array_key_exists('data', $paymentCheck) 
				&& array_key_exists('status', $paymentCheck['data']) 
				&& ($paymentCheck['data']['status'] === 'success')) {
			} else {
                return $this->errorResponse('Oops! Transaction not successful', Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }
        if($payMethod == 'bank' || $payMethod == 'paystack'){
            $create = WalletFunding::create($postData);

            if(strtolower($postData['payment_method']) == 'paystack'){
                WalletFunding::approveWalletFunding($create->id);
            }
            return $this->successResponse(WalletFunding::findOrFail($create->id));
            
        } else {
            $ref = Settings::crytpRef(strtoupper($payMethod), $userID);

            $create = CoinRemitter::createInvoice(
                $userName, 
                $postData['amount'], 
                $ref, 
                $payMethod
            );

            if($create['flag'] != 1){
                return $this->errorResponse($create['msg'], Response::HTTP_UNPROCESSABLE_ENTITY);
            } else {
                // WalletFunding::create($postData);
                $min = Config::addedTimeToApprove();
                $approveMin = CustomDateTime::addDateNoTimeZone($min.' minutes');
                cryptoPayment::create([
                    'ref_no' => $create['ref_no'],
                    'inv_id' => $create['data']['invoice_id'],
                    'user_id' => $userID,
                    'amount' => $postData['amount'],
                    'payment_method' => $payMethod,
                    'approve_time' => $approveMin
                ]);
                

                return $this->successResponse($create['data']);
            }
            
        }    
    }

}
