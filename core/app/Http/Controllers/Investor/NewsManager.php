<?php

namespace App\Http\Controllers\Investor;

use App\Classes\CustomDateTime;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Testimony;
use App\Models\Investments;
use App\Models\investmentHistory;
use App\Models\News;
use App\Models\UserNotification;
use Illuminate\Support\Facades\Auth;

class NewsManager extends Controller
{
    
    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function index(){
        $userID = Auth::user()->id;
        $records = News::forUser($userID);
        return $this->successResponse($records);
    }

    public function singleNews($id){
        $userID = Auth::user()->id;
        News::markAsRead($userID, $id);
        $result = News::findOrFail($id);
        return $this->successResponse($result);
    }

    public function notification(){
        $userID = Auth::user()->id;
        $records = UserNotification::forUser($userID);
        return $this->successResponse($records);
    }
    
    public function updateNotification($id){
        $records = UserNotification::findOrFail($id);
        if($records){
            UserNotification::where('id', $id)->update([
                'status' => 1
            ]);
        }
        $userID = Auth::user()->id;
        $records = UserNotification::forUser($userID);
        return $this->successResponse($records);
    }

}
