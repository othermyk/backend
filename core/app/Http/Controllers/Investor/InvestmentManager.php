<?php

namespace App\Http\Controllers\Investor;

use App\Classes\CustomDateTime;
use App\Classes\EmailClass;
use App\Classes\Helper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Guest\Investment;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\InvestmentPlan;
use App\Models\Investments;
use App\Models\InvestmentHistory;
use App\Models\Wallets;
use App\Models\WalletFunding;
use App\Models\User;
use App\Models\UserNotification;
use Illuminate\Support\Facades\Auth;




class InvestmentManager extends Controller
{

    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }
    
    public function plan(){
        $user_id = Auth::user()->id;
        $records = array(
            'totalFund' => WalletFunding::walletFundingPaid($user_id),
            'availableFund' => WalletFunding::availableBalance($user_id),
            'investmentCapital' => Investments::investCapital($user_id),
            'investmentPlan' => InvestmentPlan::getAllPlan()
        );
        
        return $this->successResponse($records);
    }


    public function investments(){
        $user_id = Auth::user()->id;
        $records = Investments::eachUserInv($user_id);
        return $this->successResponse($records);
    }


    public  function createInvestment(Request $request){
        $postData = $request->json()->all(); 
        $user_id = Auth::user()->id;
        $amount = $postData['amount'];
        $planID = $postData['plan_id'];
        $userInfo = User::findOrFail($user_id); 

        $invPlan = InvestmentPlan::findOrFail($planID);
        $walletBallance = WalletFunding::availableBalance($user_id);

        if($invPlan){
            if($amount <= $invPlan->max_amount){
                if($amount >= $invPlan->min_amount){
                    if($amount <= $walletBallance){

                        $result = Investments::createInvest($amount, $planID, $user_id);
                        if($result){
                            UserNotification::createRow(
                                $user_id, 
                                'Investment Creation', 
                                'You successfully create an invesment of '.$amount.' on '.$result->created_at
                            );
                            // SEND AN EMAIL
                            EmailClass::investmentCreate($userInfo, $result->id);

                            return $this->successResponse($result, Response::HTTP_CREATED);
                        }

                    } else {
                        return $this->errorResponse('Oops! your wallet balance is not sufficeint to create this investment', Response::HTTP_UNPROCESSABLE_ENTITY);
                    }
                } else {
                    return $this->errorResponse('Oops! the minimum amount for this package is '.$invPlan->min_amount, Response::HTTP_UNPROCESSABLE_ENTITY);
                }
            } else {
                return $this->errorResponse('Oops! the maximum amount for this package is '.$invPlan->max_amount, Response::HTTP_UNPROCESSABLE_ENTITY);  
            }
        } else {
            $message = "Please select investment package";
            return $this->errorResponse($message, Response::HTTP_UNPROCESSABLE_ENTITY);  
        }
    }


    public function investmentHist($investID){
        $record = array(
            'history' => InvestmentHistory::investHistory($investID),
            'investment' => Investments::findOrFail($investID),
        );
        return $this->successResponse($record);
    }


    

}
