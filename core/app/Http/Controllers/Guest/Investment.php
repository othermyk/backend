<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Models\InvestmentPlan;
use App\Traits\ApiResponder;
use App\Models\AboutUs;

class Investment extends Controller
{
    use ApiResponder;
    

    public function plan(){
        $content = array(
            'investmentPlan' => InvestmentPlan::getAllPlan(),
        );
        return $this->successResponse($content);
    }

    public function digest(){
        $get = AboutUs::where('id', 4)->first();
        return $this->successResponse($get);
    }

}
