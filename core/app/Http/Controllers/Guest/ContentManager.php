<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponder;
use App\Models\AdminBank;
use App\Models\AboutUs;
use App\Models\Faq;
use App\Models\HomeBanner;
use App\Models\HowItWorks;
use App\Models\InvestmentPlan;
use App\Models\Investments;
use App\Models\OurClient;
use App\Models\Pages;
use App\Models\Testimony;
use App\Models\User;
use App\Models\WalletFunding;
use App\Models\Withdrawal;

class ContentManager extends Controller
{
    use ApiResponder;
    

    public function banks(){
        $result = AdminBank::getActiveBanks();
        return $this->successResponse($result);
    }

    public function pages($url){
        $result = Pages::getUrl($url);
        return $this->successResponse($result);
    }
    // DB::connection('mysql2')->select(...);
    public function homeContent(){
        $content = array(
            'banner' => HomeBanner::first(),
            'aboutUs' => AboutUs::aboutUs(),
            'howItWork' => HowItWorks::activeHowItWorks(),
            'whoWeAre' => AboutUs::whoWeAre(),
            'callToAction' => AboutUs::callToAction(),
            'investmentPlan' => InvestmentPlan::firstThree(),
            'testimonies' => Testimony::firstTen(),
            'faq' => Faq::guestFaq(),
            'counter' => array(
                'investors' => User::count(),
                'totalDeposit' => WalletFunding::allCredit(),
                'investments' => Investments::where('status', '!=', 'Pending')->count(),
            ),
            'clients' => OurClient::all()
        );
        return $this->successResponse($content);
    }

    public function faq(){
        $record = Faq::guestFaq();
        return $this->successResponse($record);
    }

}
