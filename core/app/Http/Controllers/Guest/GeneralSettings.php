<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponder;
use App\Models\WebsiteSettings;
use App\Models\Configuration;
use App\Models\SocialSettings;
use App\Models\Currency;
use App\Classes\Helper;
use App\Models\BankList;
use App\Models\PaymentMethod;

class GeneralSettings extends Controller
{
    use ApiResponder;
    
    public function index(){
        Helper::cronJob();
		$result['websiteSettings'] = WebsiteSettings::first();
		$result['configuration'] = Configuration::first();
		$result['currency'] = Currency::getDefault();
		$result['social_link'] = SocialSettings::all();
		$result['allCurrency'] = Currency::all();
		$result['paymentMethod'] = PaymentMethod::getActive();
		$result['allBanks'] = BankList::get();

        return $this->successResponse($result);
    }
    
    public function home(){
        
    }
    
    public function defaultCurrency(){
        return $this->successResponse(Currency::getDefault());
    }

}
