<?php

namespace App\Http\Controllers\Guest;

use App\Classes\CustomDateTime;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Configuration;
use App\Models\UserVerification;
use Illuminate\Http\Response;
use App\Classes\Visitor;
use App\Classes\Helper;
use App\Classes\EmailClass;
use App\Classes\Settings;

class RegisterController extends Controller
{
    
    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        //
    }


    public function createRecord(Request $request){
        $postData = $request->json()->all();
        
        $checkEmail = User::checkUser($postData['email']);
        $checkUsername = User::checkUser($postData['username']);
        $checkPhone = User::checkPhone($postData['phone']);
        
        if($checkEmail){
            return $this->errorResponse('Oops! Email already exist', Response::HTTP_UNPROCESSABLE_ENTITY);
        } else if($checkUsername){
            return $this->errorResponse('Oops! Username already exist', Response::HTTP_UNPROCESSABLE_ENTITY);
        } else if($checkPhone){
            return $this->errorResponse('Oops! Phone number already exist', Response::HTTP_UNPROCESSABLE_ENTITY);
        } else {
            $postData['last_login_ip'] = Visitor::getIP();
            $postData['signup_ip'] = Visitor::getIP();
            $postData['signup_date'] = CustomDateTime::currentTime();
            $postData['last_login'] = CustomDateTime::currentTime();

            $config = Configuration::first();
            if($config->email_verify == 'Disabled'){
                $postData['email_verify'] = 1;
            } else {
                $postData['email_verify'] = 0;
            }

            $postData['password'] = app('hash')->make($postData['password']);

            $result = User::create($postData);

            if($result){
                $auth = Helper::loginHandler($result);
                if($config->email_verify == 'Enabled'){
                    $email_code = Settings::randomStrgs(6);
                    UserVerification::createVerification($result->id, $email_code);
                    EmailClass::email_verification($result, $email_code);
                }

                return $this->successResponse($auth, Response::HTTP_CREATED);
            }
        }
    }
}
