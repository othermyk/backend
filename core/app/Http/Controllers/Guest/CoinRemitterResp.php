<?php

namespace App\Http\Controllers\Guest;

use App\Classes\Config;
use App\Classes\CoinRemitter;
use App\Classes\EmailClass;
use App\Http\Controllers\Controller;
use App\Models\cryptoPayment;
use App\Models\WalletFunding;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;

class CoinRemitterResp extends Controller
{
    use ApiResponder;
    

    public function successResp($ref){
        $wallInfo = cryptoPayment::where('ref_no', $ref)->first();

        //GET INVOICE FROM COINREMITTER
        $inv = CoinRemitter::getInvoice($wallInfo->ref_no);

        if($inv['data']['status_code'] == 1){
            $set = cryptoPayment::where('ref_no', $ref)->update([
                'status' => 'Paid'
            ]);
            if($set) {
                WalletFunding::create([
                    'ref_no' => $wallInfo->ref_no,
                    'role' => 'Credit',
                    'user_id' => $wallInfo->user_id,
                    'amount' => $wallInfo->amount,
                    'payment_method' => $wallInfo->payment_method,
                    'status' => 'Approved'
                ]);
            }
        } else {
            $set = cryptoPayment::where('ref_no', $ref)->update([
                'status' => 'Expired'
            ]);
        }

        $url = Config::host().'/investor/wallet';
        header("Location: ".$url, true, 301);
        exit();
    }
    
    public function failedResp($ref){
        cryptoPayment::where('ref_no', $ref)->delete();
    }





}
