<?php

namespace App\Http\Controllers\Guest;

use App\Classes\CoinRemitter;
use App\Classes\Settings;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserVerification;
use App\Classes\CustomDateTime;
use App\Classes\Helper;
use App\Classes\EmailClass;
use App\Classes\Paystack;
use App\Models\Configuration;
use Illuminate\Http\Response;

class LoginController extends Controller
{
    
    use ApiResponder;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        //
    }

    public function checking(){
        $config = CoinRemitter::createInvoice('Johndoe', '3000', 'LITHS87654GHJ', 'litecoin');
        return $this->successResponse($config['data']['invoice_id']);
    }

    // public function checkingTran($transID){
    //     $config = Paystack::transactionRequest($transID);
    //     return $this->successResponse($config);
    // }


    public function loginHere(Request $request){
        $postData = $request->json()->all();

        $result = User::checkUser($postData['user']);

        if($result){
            if($result->status == 'Blocked' || $result->status == 'Suspended') {
                return $this->errorResponse('This account has been Blocked / Suspended. Please Contact support.', Response::HTTP_UNPROCESSABLE_ENTITY);
            } 
            else if ($this->verifyPassword($postData['password'], $result->password)) {
                // $email_code = Settings::randomStrgs(6);
                // UserVerification::createVerification($result->id, $email_code);
                // EmailClass::loginVerification($result, $email_code);
                // return $result;

                User::where('id', $result->id)->update([
                    'last_login' =>  CustomDateTime::currentTime()
                ]);
                return Helper::loginHandler($result);
            }
            else {
                return $this->errorResponse('Oops! Password and Email does not match, please try it again.', Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } else {
            return $this->errorResponse('Oops! No record found with your entry.', Response::HTTP_UNPROCESSABLE_ENTITY);
        }  
    }


    // public function loginHere(Request $request){
    //     $postData = $request->json()->all();

    //     $result = User::checkUser($postData['email']);
    //     $code = $postData['email_code'];

    //     if($result){
    //         if($result->status == 'Blocked' || $result->status == 'Suspended') {
    //             return $this->errorResponse('This account has been Blocked / Suspended. Please Contact support.', Response::HTTP_UNPROCESSABLE_ENTITY);
    //         } 
    //         else {

    //             $verifyCode = UserVerification::where('email_code', $code)->where('user_id', $result->id)->first();
    //             if($verifyCode){
    //                 User::where('id', $result->id)->update([
    //                     'last_login' =>  CustomDateTime::currentTime()
    //                 ]);
    //                 return Helper::loginHandler($result);
    //             } else {
    //                 return $this->errorResponse('Oops! incorrect code, please retype.', Response::HTTP_UNPROCESSABLE_ENTITY);
    //             }
                
    //         }
    //     } else {
    //         return $this->errorResponse('Oops! No record found with your entry.', Response::HTTP_UNPROCESSABLE_ENTITY);
    //     }  
    // }


    public function home(){
        // return view('welcome', [
        //     'users' => User::get()
        // ]);
    }


}
