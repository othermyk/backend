<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Classes\CustomDateTime;

class cryptoPayment extends Model
{

    
    
    protected $fillable = [
        'ref_no',
        'user_id',
        'inv_id',
        'amount',
        'payment_method',
        'approve_time',
        'status',
    ];

    protected $hidden = [
       
    ];

    public static function singleByRef($refNo){
        return self::where('ref_no', $refNo)->first();
    }



    // public static function autoApproveWallet() {
    //     $currentTime = CustomDateTime::currentTimeNotimeZone();
    //     $result = self::whereRaw("status = 'Paid' AND amount != 0 AND approve_time <= '$currentTime'")
    //         ->orderBy('id', 'ASC')->get();

    //     if(count($result) > 0){
    //         foreach ($result as $value) {
    //             $set = self::where('id', $value->id)->update([
    //                 'status' => 'Completed'
    //             ]);
    //             if($set){
    //                 WalletFunding::where('ref_no', $value->ref_no)->update([
    //                     'status' => 'Approved'
    //                 ]);
    //             }

    //             //Notification
    //             UserNotification::createRow(
    //                 $value->user_id,
    //                 'Wallet Funding Approval', 
    //                 'Your wallet has been credited with '.$value->amount
    //             );
    //         }
    //     }

    //     return;        
    // } 




}
