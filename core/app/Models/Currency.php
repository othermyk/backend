<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{

    use HasFactory;
    
    protected $fillable = [
        'c_default',
        'country',
        'code',
        'symbol',
    ];

    public static function getDefault(){
        return self::where('c_default', 1)->first();
    }

    public static function setDefault($id){
        $def = self::getDefault();
        $toUpdate = self::findOrFail($id);
        self::where('id', $def->id)->update([
            'c_default' => 0
        ]);

        if($toUpdate){
            self::where('id', $id)->update([
               'c_default' => 1 
            ]);
        }
        $newDef = self::all();
        return $newDef;
    }

}
