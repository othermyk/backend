<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use App\Classes\CustomDateTime;

class WalletFunding extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    
    
    protected $fillable = [
        'ref_no',
        'role',
        'user_id',
        'amount',
        'pop',
        'payment_method',
        'date_approved',
        'status',
    ];

    protected $hidden = [
       
    ];

    public static function getForUser($user_id){
        return self::where('user_id', $user_id)->get();
    }

    public static function walletFundingStatus($user_id, $status){
        return self::where('user_id', $user_id)->where('role', 'Credit')->where('status', $status)->sum('amount');
    }
    
    public static function walletFundingPaid($user_id){
        return self::where('user_id', $user_id)->where('role', 'Credit')->sum('amount');;
    }

    public static function allRecordCounts($status){
        if($status == 'all'){
            return self::count();
        } else {
            return self::where('status', $status)->count();
        }
    }
    
    public static function allRecords($status, $take, $offset){
       if($status == 'all'){
        return self::take($take)->offset($offset)
            ->select('wallet_fundings.*', 'users.first_name', 'users.last_name')
            ->leftJoin('users', 'users.id', 'wallet_fundings.user_id')
            ->where('role', 'Credit')->orderBy('id', 'DESC')->get();
       } else {
        return self::take($take)->offset($offset)
            ->select('wallet_fundings.*', 'users.first_name', 'users.last_name')
            ->leftJoin('users', 'users.id', 'wallet_fundings.user_id')
            ->where('wallet_fundings.status', $status)
            ->where('role', 'Credit')->orderBy('id', 'DESC')->get();
       }
    }

    public static function approveWalletFunding($id){
        $single = self::findOrFail($id);
        $approve = self::where('id', $id)->update([
            'status' => 'Approved',
            'date_approved' => CustomDateTime::currentTime()
        ]);
        if ($approve) {
            UserNotification::createRow(
                $single->user_id, 
                'Wallet Funding Approval', 
                'Your wallet has been credited with '.$single->amount
            );
        }
        return $approve;
    }


    public static function createRow($userID, $amount, $role, $payMethod, $pop=null){
        return self::create([
            'role' => $role,
            'user_id' => $userID,
            'amount' => $amount,
            'payment_method' => $payMethod,
            'pop' => $pop
        ]);
    }


    public static function availableBalance($userID){
        return self::totalCredit($userID) - self::totalDebit($userID);
    }

    public static function totalDebit($userID){
        return self::where('role', 'Debit')->where('user_id', $userID)->sum('amount');
    }

    public static function totalCredit($userID){
        return self::where('role', 'Credit')->where('status', 'Approved')->where('user_id', $userID)->sum('amount');
    }


    public static function userWalletFund($user_id){
        return self::where('user_id', $user_id)->orderBy('id', 'DESC')->get();
    }
    
    public static function adminNotice(){
        return self::select('wallet_fundings.*', 'users.first_name', 'users.last_name')
            ->leftJoin('users', 'users.id', 'wallet_fundings.user_id')
            ->where('wallet_fundings.status', 'Pending')->where('role', 'Credit')->take(1)->get();
    }


    public static function userWallFundAdmin($userID, $take, $offset){
        return self::where('user_id', $userID)->take($take)->offset($offset)->orderBy('id', 'DESC')->get();
    }
    
    public static function userWallFundAdminCount($userID){
        return self::where('user_id', $userID)->count();
    }


    public static function allCredit(){
        return self::where([
            ['role', 'Credit'], ['status', 'Approved']
        ])->sum('amount');
    }




}
