<?php

namespace App\Models;

use App\Classes\CustomDateTime;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use App\Classes\EmailClass;

class Withdrawal extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    
    
    protected $fillable = [
        'id',
        'user_id',
        'wallet_id',
        'amount',
        'pay_method',
        'pay_to',
        'approve_date',
        'status',
    ];

    
    
    protected $hidden = [
        
    ];


    public static function updateWithStatus($id){
        $approve = self::where('id', $id)->update([
            'status' => 'Approved',
            'approve_date' => CustomDateTime::currentTime()
        ]);

        if($approve){
            $with = self::findOrFail($id);
            $userInfo = User::findOrFail($with->user_id);
            UserNotification::createRow(
                $with->user_id, 
                'Withdrawal Approval', 
                'Your withdrawal request initiated on has been successfully approved'
            );
            $check = EmailClass::WithdrawalApproval($userInfo, $with);
            return $check;
            // return self::findOrFail($id);
        } else {
            return null;
        }
    }

    public static function userWithddrawal($user_id, $take, $offset){
        return self::take($take)->offset($offset)->where('user_id', $user_id)->orderBy('id', 'DESC')->get();
    }

    public static function userCount($user_id){
        return self::where('user_id', $user_id)->count();
    }

    public static function allRecordCounts($status){
        if($status == 'all'){
            return self::count();
        } else {
            return self::where('status', ucwords($status))->count();
        }
    }
    
    public static function allRecords($status, $take, $offset){
        if($status == 'all'){
            return self::take($take)->offset($offset)
            ->select('withdrawals.*', 'users.first_name', 'users.last_name')
            ->leftJoin('users', 'users.id', 'withdrawals.user_id')->orderBy('id', 'DESC')->get();
        } else {
            return self::take($take)->offset($offset)
            ->select('withdrawals.*', 'users.first_name', 'users.last_name')
            ->leftJoin('users', 'users.id', 'withdrawals.user_id')
            ->where('status', ucwords($status))->orderBy('id', 'DESC')->get();
        }
    }

    public static function totalWithdrawal(){
        return self::where('status', 'Approved')->sum('amount');
    }

    public static function userWith($user_id){
        return self::where('user_id', $user_id)->orderBy('id', 'DESC')->get();
    }


    public static function adminNotice(){
        return self::select('withdrawals.*', 'users.first_name', 'users.last_name')
            ->leftJoin('users', 'users.id', 'withdrawals.user_id')
            ->where('withdrawals.status', 'Pending')->take(1)->get();
    }


}
