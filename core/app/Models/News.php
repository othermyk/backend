<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{

    use HasFactory;
    
    protected $fillable = [
        'title',
        'contents',
        'user_seen',
        'status',
    ];


    public static function forUser($userID){
        $getAll = self::where('status', 1)->get();

        $newNews = array();
        foreach($getAll as $value){
            $value['isRead'] = self::checkIsRead($userID, $value->id);

            array_push($newNews, $value);
        }
        return $newNews;
    }


    public static function checkIsRead($userID, $newsID){
        $getNew = self::findOrFail($newsID);
        $turnToArray = explode(',', $getNew->user_seen);
        foreach($turnToArray as $value){
            if($value == $userID){
                return 1;
            }
        }
        return 0;
    }

    public static function markAsRead($userID, $id){
        $getNew = self::findOrFail($id);
        $check = self::checkIsRead($userID, $id);
        if($check == 0){
            if($getNew->user_seen){
                $toUpdate = $getNew->user_seen.','.$userID;
            } else {
                $toUpdate = $userID;
            }
            News::where('id', $id)->update([
                'user_seen' => $toUpdate
            ]);
        }
    }
    

}
