<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use App\Traits\ApiResponder;
use Illuminate\Http\Response;

class UserBank extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;
    use ApiResponder;
   
    protected $fillable = [
        'id',
        'user_id',
        'account_name',
        'account_number',
        'bank_name',
        'account_type',
        'status',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];



    public static function singleByUserId($user_id){
        return self::where('user_id', $user_id)->first();
    }
    
    public static function allUserBank($user_id){
        return self::where('user_id', $user_id)->orderBy('id', 'desc')->get();
    }


    public static function updateBank($postData){
        $update = self::where('id', $postData['id'])->update([
            'account_name' => $postData['account_name_edit'],
            'account_number' => $postData['account_number_edit'],
            'bank_name' => $postData['bank_name_edit'],
            'account_type' => $postData['account_type_edit'],
        ]);
        $record = self::findOrFail($postData['id']);
        
        return $record;
    }
}
