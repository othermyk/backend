<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Classes\EmailClass;
use App\Models\User;
use App\Classes\CustomDateTime;

class Investments extends Model
{

    use HasFactory;
    
    protected $fillable = [
        'user_id',
        'plan_detail',
        'duration',
        'amount',
        'roi_perc',
        'roi',
        'total_earn',
        'start_date',
        'end_date',
        'status',
    ];


    public static function eachUserInv($loginID){
        return self::where('user_id', $loginID)->orderBy('id', 'DESC')->get();
    }

    public static function firstThree(){
        return self::orderBy('id', 'DESC')->take(3)->get();
    }


    public static function getInvForAdmin($take, $offset, $status){
        if($status == 'all'){
            return self::select('investments.*', 'users.first_name', 'users.last_name')
            ->leftJoin('users', 'users.id', '=', 'investments.user_id')
            ->take($take)->offset($offset)->orderby('investments.id', 'DESC')->get();
        } else {
            return self::select('investments.*', 'users.first_name', 'users.last_name')
            ->leftJoin('users', 'users.id', '=', 'investments.user_id')
            ->where('investments.status', $status)
            ->take($take)->offset($offset)->orderby('investments.id', 'DESC')->get();
        }
    }

    public static function countInv($status){
        if($status == 'all'){
            return self::count();
        } else {
            return self::where('status', $status)->count();
        }
    }

    public static function investCapital($userID){
        return self::where('user_id', $userID)->sum('amount');
    }



    public static function createInvest($amount, $planID, $userID){
        $plan = InvestmentPlan::findOrFail($planID);
        $roi = ($amount * $plan->roi) / 100;
        if($plan->roi_duration == 'hourly' || $plan->roi_duration == 'daily'){
            $durLen = 'days';
        } else if($plan->roi_duration == 'weekly'){
            $durLen = 'weeks';
        } else if($plan->roi_duration == 'monthly'){
            $durLen = 'months';
        } else if($plan->roi_duration == 'yearly'){
            $durLen = 'years';
        }

        $create = self::insertGetId([
            'user_id' => $userID,
            'amount' => $amount,
            'roi_perc' => $plan->roi,
            'roi' => $roi,
            'duration' => $plan->duration.' '.$durLen,
            'plan_detail' => $plan,
            'created_at' => CustomDateTime::currentTime(),
            'updated_at' => CustomDateTime::currentTime()
        ]);

        if($create){
            WalletFunding::createRow($userID, $amount, 'Debit', 'invest');
            $otherFunc = self::approveInvestment($create);
            return $otherFunc;
        } else {
            return null;
        }

    }


    public static function approveInvestment($investID){
        $result = self::findOrFail($investID);
        $invPlan = json_decode($result->plan_detail);
        $user = User::findOrFail($result->user_id);

        if($result){
            $endDate = null;
            $handlerTime = null;
            if($invPlan->roi_duration == 'hourly'){
                $handlerTime = 'hours';
                $theLoop = $invPlan->duration * 24;
                $endDate = CustomDateTime::addDate($invPlan->duration.' days');
            } else if($invPlan->roi_duration == 'daily'){
                $handlerTime = 'days';
                $theLoop = $invPlan->duration;
                $endDate = CustomDateTime::addDate($invPlan->duration.' days');
            } else if($invPlan->roi_duration == 'weekly'){
                $handlerTime = 'weeks';
                $theLoop = $invPlan->duration;
                $endDate = CustomDateTime::addDate($invPlan->duration.' weeks');
            } else if($invPlan->roi_duration == 'monthly'){
                $handlerTime = 'months';
                $theLoop = $invPlan->duration;
                $endDate = CustomDateTime::addDate($invPlan->duration.' months');
            } else if($invPlan->roi_duration == 'yearly'){
                $handlerTime = 'years';
                $theLoop = $invPlan->duration;
                $endDate = CustomDateTime::addDate($invPlan->duration.' years');
            }
            $TotalEarn = 0;
            for ($i=0; $i < $theLoop; $i++) {
                $star = $i + 1;                

                $theDur = $star;

                $TotalEarn +=$result->roi;

                $postData['invest_id'] = $investID;
                $postData['user_id'] = $result->user_id;
                $postData['level'] = $star;
                $postData['amount'] = $result->roi;
                $postData['next_credit_date'] = self::dateHandler(CustomDateTime::currentTime(), $theDur.' '.$handlerTime);

                InvestmentHistory::create($postData);              
            }

            self::where('id', $investID)->update([
                'start_date' => CustomDateTime::currentTime(),
                'end_date' => $endDate,
                'total_earn' => $TotalEarn,
                'status' => 'Active'
            ]);

            if($user->referral){
                self::refCommission($result);
            }

            return self::findOrFail($investID);
        }
    }

    private static function dateHandler($existDate, $string) {
        return Date('Y-m-d H:i:s', strtotime($existDate ."+".$string));
    }



    public static function refCommission($invest){
        //user who is paying
        $userInfo = User::findOrFail($invest->user_id);

        //get the configuration set by admin
        $config = Configuration::first();

        //get the downlines of the user who is paying
        $downlines = User::userUplines($userInfo->id);

        $i = 0;

        $decodeRefBonus = json_decode($config->ref_bonus);
        $turnComma = null;
        foreach($decodeRefBonus as $value){
            $turnComma .= $value->bonus.',';
        }
        $getEach = array_filter(explode(',', $turnComma));


        foreach ($downlines as $value) {
            //user who will receive bonus
            $ref = User::getReferral($value->username);

            //count the number of time this referral has receive referral bonus from this user.
            $reCounts = ReferralBonus::rewardCounts($ref->id, $userInfo->id);

            if($reCounts < $config->referral_reward_count){
                $i++;

                if ($i == 1) {
                    $bonus = ($invest->amount / 100) * $getEach[$i - 1];
                    $remark = 'referral';
                    $mssg = 'Referral Commission from '.$userInfo->first_name.' '.$userInfo->last_name;
                } else {
                    $bonus = ($invest->amount / 100) * $getEach[$i - 1];
                    $remark = 'referral level-'.$i;
                    $mssg = 'referral level-'.$i.' Commission from '.$userInfo->first_name.' '.$userInfo->last_name;
                }

                if ($value AND $value->id) {       
                    ReferralBonus::createdRecord(
                        $value->id, 
                        $invest->user_id, 
                        $invest->id, 
                        $invest->amount,
                        $bonus,
                        $remark,
                        'credit'
                    );

                    Wallets::createRow(
                        $value->id,
                        $bonus,
                        'credit',
                        $mssg,
                        $remark
                    );
                }
            }
        }



        // $inv = self::findOrFail($invest->id);
        // $reCounts = ReferralBonus::rewardCounts($ref->id, $userInfo->id);
        // $config = Configuration::first();

        // if($ref->user_type == 'Investor'){
        //     $refBonus = $config->investor_ref_bonus;
        // } else {
        //     $refBonus = $config->marketer_ref_bonus;
        // }

        // if($refBonus > 0 AND $reCounts < 1){
        //     $bonus = ($inv->total_paid * $refBonus) / 100;

        //     $remark = 'Referral Commission from '.$userInfo->username.' investment';
        //     if ($bonus) {       
        //         $createBonu = ReferralBonus::createdRecord(
        //             $userInfo->id, 
        //             $ref->id, 
        //             $inv->id, 
        //             $inv->total_paid,
        //             $bonus,
        //             $remark,
        //             'credit'
        //         );

        //         if($createBonu){
        //             Wallets::createRow(
        //                 $ref->id, $bonus, 'Credit', $remark, 'Referral Bonus'
        //             );

        //             UserNotification::createRow(
        //                 $ref->id, 'Referral Bonus', $remark
        //             );
        //         }
        //     }
        // }
    }


    public static function topTenAdmin(){
        return self::select('investments.*', 'users.first_name', 'users.last_name')
            ->leftJoin('users', 'users.id', '=', 'investments.user_id')
            ->orderby('investments.id', 'DESC')->take(10)->get();
    }

    public static function totalInvestment(){
        return self::where('status', '!=', 'Pending')->sum('total_paid');
    }


    public static function adminNotice(){
        return self::select('investments.*', 'users.first_name', 'users.last_name')
            ->leftJoin('users', 'users.id', 'investments.user_id')
            ->where('investments.status', 'Pending')->take(1)->get();
    }



    public static function updateCompleteInvestment() {
        $currentTime = CustomDateTime::currentDate();
        $result = self::whereRaw("status = 'Active' AND end_date <= '$currentTime'")
            ->orderBy('end_date', 'ASC')->get();

        if(count($result) > 0){
            foreach ($result as $value) {
                self::where('id', $value->id)->update([
                    'status' => 'Completed'
                ]);
            }
        }
        return;        
    } 



}
