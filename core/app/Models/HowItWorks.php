<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HowItWorks extends Model
{

    use HasFactory;
    
    protected $fillable = [
        'title',
        'contents',
        'icon',
        'status',
    ];


    public static function activeHowItWorks(){
        return self::where('status', 1)->get();
    }

}
