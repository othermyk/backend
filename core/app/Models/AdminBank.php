<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminBank extends Model
{

    use HasFactory;
    
    protected $fillable = [
        'account_name',
        'account_number',
        'bank',
        'account_type',
        'currency',
        'status',
    ];


    public static function getActiveBanks(){
        return self::where('status', 1)->get();
    }

}
