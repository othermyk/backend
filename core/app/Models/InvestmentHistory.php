<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Classes\CustomDateTime;

class InvestmentHistory extends Model
{

    use HasFactory;
    
    protected $fillable = [
        'invest_id',
        'user_id',
        'level',
        'amount',
        'next_credit_date',
        'status',
    ];


    public static function investHistory($investID){
        return self::where('invest_id', $investID)->get();
    }

    public static function totalIncome($userID){
        return self::where('user_id', $userID)->where('status', 1)->sum('amount');
    }


    public static function releaseROIFunds() {
        $currentTime = CustomDateTime::currentTime();
        $result = self::whereRaw("status = 0 AND amount != 0 AND next_credit_date <= '$currentTime'")
            ->orderBy('next_credit_date', 'ASC')->get();

        if(count($result) > 0){
            foreach ($result as $value) {
                $level = $value->level;
                $inv = Investments::findOrFail($value->invest_id);
                $invPlan = json_decode($inv->plan_detail);
                if($invPlan->roi_duration == 'daily'){
                    $dur = 'day';
                } else {
                    $dur = substr($invPlan->roi_duration, 0, -2);
                }
                Wallets::createRow(
                    $value->user_id, 
                    $value->amount, 
                    'Credit',
                    'ROI Credit for '.strtoupper($dur).'-'.$level,
                    'Investment ROI'
                );
                
                //Update
                self::where('id', $value->id)->update([ 'status' => 1 ]);

                // $takeUser = User::singleByID($value->login_id);

                //Notification
                UserNotification::createRow(
                    $value->user_id, 
                    'Referral Bonus', 
                    'Your wallet has been credited with '.$value->amount.' ROI for MONTH-'.$level
                );
            }
        }

        return;        
    } 


    public static function walletCredit($loginID, $amt, $currency, $level) {
        return  Wallets::createRecord($loginID, $amt, $currency, 'Credit', 0, 'ROI Credit for MONTH-'.$level,'ROI Credit');
    } 



}
