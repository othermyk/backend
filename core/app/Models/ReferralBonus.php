<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReferralBonus extends Model
{

    use HasFactory;
    
    protected $fillable = [
        'user_id',
        'referree_id',
        'invest_id',
        'amount',
        'bonus',
        'status',
        'remark',
    ];


    public static function rewardCounts($referreeID, $userID){
        return self::where('user_id', $referreeID)->where('referree_id', $userID)->count();
    }

    public static function createdRecord($userID, $refID, $invID, $amount, $bonus, $remark, $status){
        return self::create([
            'user_id' => $userID,
            'referree_id' => $refID,
            'invest_id' => $invID,
            'amount' => $amount,
            'bonus' => $bonus,
            'remark' => $remark,
            'status' => $status
        ]);
    }



    public static function sumRefCom($userID){
        return self::where('user_id', $userID)->where('status', 'Credit')->sum('bonus');
    }


}
