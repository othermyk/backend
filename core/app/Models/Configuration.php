<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{

    use HasFactory;
    
    protected $fillable = [
        'ref_bonus',   
        'min_withdrawal',   
        'referral_reward_count', 
        'email_verify', 
        'sms_verify', 
        'sms_note', 
        'sms_gateway_url',
        'pay_instruction',
    ];




    public static function referralLevel(){
        $config = self::first();
        $decodeRefBonus = json_decode($config->ref_bonus);
        $count = 0;
        foreach($decodeRefBonus as $value){
            $count++;
        }
        return $count;
    }

}
