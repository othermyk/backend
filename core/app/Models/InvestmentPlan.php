<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvestmentPlan extends Model
{
    
    
    use HasFactory;

    protected $fillable = [
        'name',
        'min_amount',
        'max_amount',
        'duration',
        'roi_duration',
        'roi',
        'status'
    ];

    // public static function allActivePlan(){
    //     return self::where('status', 1)->get();
    // }

    public static function updateInvestSlot($id, $slot){
        $inPlan = self::findOrFail($id);
        if(($inPlan->invested_slot + $slot) == $inPlan->slot){
            $status = 'Sold Out';
        } else {
            $status = $inPlan->status;
        }
        return self::where('id', $id)->update([
            'invested_slot' => $inPlan->invested_slot + $slot,
            'status' => $status
        ]);
    }

    public static function getAllPlan(){
        $plan = self::where('status', '!=', 'Pending')->get();
        $newPlan = array();
        foreach($plan as $value){
            $value['slot'] = $value->slot - $value->invested_slot;

            array_push($newPlan, $value);
        }

        return $newPlan;
    }

    public static function firstThree(){
        return self::where('status', '!=', 'Pending')->orderBy('id', 'DESC')->get();
    }


    public static function getInvForAdmin($take, $offset, $status){
        if($status == 'all'){
            return self::take($take)->offset($offset)->orderby('id', 'DESC')->get();
        } else {
            return self::where('status', $status)
            ->take($take)->offset($offset)->orderby('id', 'DESC')->get();
        }
    }

    public static function countInv($status){
        if($status == 'all'){
            return self::count();
        } else {
            return self::where('status', $status)->count();
        }
    }

    public static function activeInvesmentPlan(){
        return self::where('status', 'Now Selling')->count();
    }
}
