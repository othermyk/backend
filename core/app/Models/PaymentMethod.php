<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{

    use HasFactory;
    
    protected $fillable = [
        'name',
        'name_url',
        'api_key',
        'password',
        'address',
        'status',
    ];


    public static function getActive(){
        return self::where('status', 1)->get();
    }
    
    public static function getAll(){
        return self::get();
    }

    
    

}
