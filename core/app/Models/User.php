<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    
    
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'email',
        'password',
        'api_token',
        'phone',
        'gender',
        'referral',
        'marital_status',
        'date_of_birth',
        'occupation',
        'address',
        'state',
        'lga',
        'next_of_kin',
        'next_of_kin_phone',
        'photo',
        'proof_of_identity',
        'proof_of_identity_status',
        'email_verify',
        'status',
        'signup_ip',
        'signup_date',
        'last_login_ip',
        'last_login',
    ];

    


    protected $hidden = [
        'password',
        'api_token'
    ];


    public static function updatePassword($password, $user_id){
        return self::where('id', $user_id)->update([
            'password' => app('hash')->make($password)
        ]);
    }

    public static function checkUser($checkUser){
        return self::where('username', $checkUser)->orWhere('email', $checkUser)->orWhere('phone', $checkUser)->first();
    }

    public static function emailCheck($checkUser){
        return self::Where('email', $checkUser)->first();
    }
    
    public static function checkPhone($phone){
        return self::where('phone', $phone)->first();
    }

    public static function logout($user_id){
        return self::where('id', $user_id)->update([
            'api_token' => null
        ]);
    }

    public static function blockUser(){
        return self::where('status', 'Blocked')->orWhere('status', 'Suspended')->count();
    }


    public static function getUserByStatus($role, $limit, $offset){
        if($role == 'all'){
            return self::take($limit)->offset($offset)->orderBy('id', 'DESC')->get();
        } else {
            return self::where('status', ucwords($role))->take($limit)->offset($offset)->orderBy('id', 'DESC')->get();
        }
    }

    public static function getUserByCount($role){
        if($role == 'all'){
            return self::count();
        } else {
            return self::where('status', ucwords($role))->count();
        }
    }

    public static function accountAction($userID, $action){
        return self::where('id', $userID)->update([
            'status' => $action,
            'email_verify' => 1,
        ]); 
    }

    public static function getReferral($userName){
        return self::where('username', $userName)->first();
    }


    public static function allReferral($userName, $limit, $offset){
        return self::where('referral', $userName)->take($limit)->offset($offset)->orderBy('id', 'DESC')->get();
    }
    
    public static function countAllReferral($userName){
        return self::where('referral', $userName)->count();
    }


    public static function topTenAdmin(){
        return self::orderby('id', 'DESC')->take(10)->get();
    }



    // public static function userReferral($username, $take, $offset){
    //     return self::where('referral', $username)->take($take)->offset($offset)->orderBy('id', 'DESC')->get();
    // }
    
    
    public static function countRef($username){
        return self::where('referral', $username)->count();
    }



    public static function userUplines($userID){
        $referral_level = Configuration::referralLevel();
        $user = self::findOrFail($userID);
        $ref = self::where('username', $user->referral)->first();

        $uplines = array();
        array_push($uplines, $ref);

        for ($i=1; $i < $referral_level; $i++) {
            if($uplines[$i-1]->referral){
                $ref = self::where('username', $uplines[$i-1]->referral)->first();
                array_push($uplines, $ref);
            } else {
                return $uplines;
            }
        }

        return $uplines;
    }




    //referral count list
    public static function userDownliners($username, $level=1, $limit=1, $page=1){        
        $recruiters = [$username];
        for ($iteration=1; $iteration <= $level ; $iteration++) { 

            //ensure the pagination is on the last result set
            if ($iteration == $level) {
                $page = $page; 
                $per_page = $limit;
            }else{
                $page = 1; 
                $per_page = 20;
            }

            $downlines = User::userDownlinersOptimised($recruiters, $page, $per_page);
            $recruiters =  $downlines['usernames'];
        }

        return $downlines ;
    }

    public static function userDownlinersOptimised(array $recruiters=[], $page=1, $per_page = 20){
        $skip = ($page - 1)* $per_page;

        $sql_query = self::whereIn('referral' , $recruiters);
        $downlines['total'] = $sql_query->count();

        if ($per_page == 20) {
            $downlines['usernames'] = $sql_query->get()->pluck(['username'])->toArray();
            $downlines['list'] = $sql_query->orderBy('username', 'DESC')->get()->toArray();
            $page = 1;
        }else{
            $downlines['usernames'] = $sql_query->offset($skip)->take($per_page)
                ->orderBy('username', 'DESC')->get()->pluck(['username'])->toArray();

            $downlines['list'] = $sql_query->offset($skip)->take($per_page)
                ->orderBy('username', 'DESC')->get()->toArray();

        }

        return $downlines;
    }


    public static function adminSearch($keyword){
        $get = self::whereRaw("first_name LIKE '%$keyword%' OR last_name LIKE '%$keyword%' OR username LIKE '%$keyword%' OR email LIKE '%$keyword%'")->get();
        return $get;
    }
    
    public static function adminSearchCount($keyword){
        $get = self::whereRaw("first_name LIKE '%$keyword%' OR last_name LIKE '%$keyword%' OR username LIKE '%$keyword%' OR email LIKE '%$keyword%'")->count();
        return $get;
    }
    





}
