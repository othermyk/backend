<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use App\Traits\ApiResponder;
use Illuminate\Http\Response;

class UserCrypto extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;
    use ApiResponder;
   
    protected $fillable = [
        'id',
        'user_id',
        'crypto',
        'wallet_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];



    public static function singleByUserId($user_id){
        return self::where('user_id', $user_id)->first();
    }

    public static function singleByCrypto($user_id, $crypto){
        return self::where([
            ['user_id', $user_id], ['crypto', $crypto]
        ])->first();
    }

    public static function userList($user_id){
        return self::where('user_id', $user_id)->get();
    }
    

    public static function addOrUpdate($data, $userId){
        $crypto = strtolower($data['crypto']);
        $check = self::where([
            ['user_id', $userId], ['crypto', $crypto]
        ])->first();


        if($check){
            //update
            self::where([
                ['user_id', $userId], ['crypto', $crypto]
            ])->update([
                'wallet_id' => $data['wallet_id']
            ]);
        } else {
            //create
            self::create([
                'user_id' => $userId,
                'wallet_id' => $data['wallet_id'],
                'crypto' => $data['crypto']
            ]);
        }
        
        $record = self::userList($userId);
        
        return $record;
    }
}
