<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Wallets extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    
    
    protected $fillable = [
        'id', 
        'user_id',
        'amount',
        'status',
        'note',
        'source',
    ];

    
    protected $hidden = [
        
    ];


    public static function createRow($user_id, $amount, $status, $note, $source){
        return self::create([
            'user_id' => $user_id,
            'amount' => $amount,
            'status' => $status,
            'note' => $note,
            'source' => $source,
        ]);
    }


    public static function getBalance($user_id){
        return self::totalCredit($user_id) - self::totalDebit($user_id);
    }

    public static function totalCredit($user_id){
        return self::where('user_id', $user_id)->where('status', 'Credit')->sum('amount');
    }

    public static function totalDebit($user_id){
        return self::where('user_id', $user_id)->where('status', 'Debit')->sum('amount');
    }

    public static function userWallet($user_id){
        return self::where('user_id', $user_id)->orderBy('id', 'DESC')->get();
    }

    public static function userWalletFirstTen($user_id){
        return self::where('user_id', $user_id)->orderBy('id', 'DESC')->take(5)->get();
    }

    public static function userWalletAdmin($userID, $limit, $offset){
        return self::where('user_id', $userID)->take($limit)->offset($offset)->orderBy('id', 'DESC')->get();
    }
    
    public static function userWalletAdminCount($userID){
        return self::where('user_id', $userID)->count();
    }
}
