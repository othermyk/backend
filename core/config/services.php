<?php

    return [
        'investment' => [
            'base_uri' => env('INVESTMENT_SERVICE_BASE_URL'),
            'secret' => env('INVESTMENT_SERVICE_SECRET')
        ],
        
        'contents' => [
            'base_uri' => env('CONTENTS_SERVICE_BASE_URL'),
            'secret' => env('CONTENTS_SERVICE_SECRET')
        ],
    ];





?>