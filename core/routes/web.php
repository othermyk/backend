<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () {
    return $router->app->version();
});

$router->group(['middleware' => 'auth'], function() use($router){
    $router->post('/user/account-settings/profile', 'Investor\AccountSettings@updateProfile');
    $router->get('/user/account-settings/bank', 'Investor\AccountSettings@bankInfo');
    $router->post('/user/account-settings/update-bank', 'Investor\AccountSettings@updateBankInfo');
    $router->post('/user/account-settings/add-bank', 'Investor\AccountSettings@addBankInfo');
    $router->post('/user/account-settings/change-password', 'Investor\AccountSettings@changePassword');

    
    $router->post('/user/account-settings/update-crypto', 'Investor\AccountSettings@updateCryptoInfo');
    $router->get('/user/account-settings/user-crypto', 'Investor\AccountSettings@getUserCrypto');



    $router->get('/user/withdrawal/{limit}/{page}', 'Investor\WithdrawalManager@withdrawals');
    $router->post('/user/withdrawal/request', 'Investor\WithdrawalManager@submitWithRequest');
    

    $router->get('/user/dashboard', 'Investor\Dashboard@index');
    
    $router->get('/user/referral/{limit}/{page}', 'Investor\ReferrallManager@index');
    
    $router->get('/user/wallet', 'Investor\WalletManager@allFunds');
    $router->post('/user/wallet/credit', 'Investor\WalletManager@creditWallet');

    $router->put('/user/account-settings/logout', 'Investor\AccountSettings@logout');
    $router->post('/user/account-settings/email-verify', 'Investor\AccountSettings@emailVerification');
    $router->get('/user/account-settings/resend-token', 'Investor\AccountSettings@resendToken');

    // USER INVESTMENT
    $router->group(['user' => 'investment'], function() use($router){
        $router->get('/user/investment/plan', 'Investor\InvestmentManager@plan');
        $router->post('/user/investment/create', 'Investor\InvestmentManager@createInvestment');
        $router->get('/user/investment', 'Investor\InvestmentManager@investments');
        $router->get('/user/investment/history/{investID}', 'Investor\InvestmentManager@investmentHist');
        // $router->get('/user/investment/generateInvoice', 'Investor\PDFController@createPDF');
    });

    // USER TESTIMONIAL
    $router->group(['user' => 'investment'], function() use($router){
        $router->get('/user/testimony', 'Investor\TestimonyManager@getTestimony');
        $router->post('/user/testimony/create', 'Investor\TestimonyManager@createTestimony');
    });

    // USER NEWS AND NOTIFICATION
    $router->group(['user' => 'investment'], function() use($router){
        $router->get('/user/news', 'Investor\NewsManager@index');
        $router->get('/user/news/single/{id}', 'Investor\NewsManager@singleNews');
        $router->get('/user/notification', 'Investor\NewsManager@notification');
        $router->get('/user/notification/update/{id}', 'Investor\NewsManager@updateNotification');
    });

     // USER SUPPORT
     $router->group(['user' => 'support'], function() use($router){
        $router->get('/user/support/{status}/{limit}/{page}', 'Investor\UserSupport@index');
        $router->get('/user/support/single/{id}', 'Investor\UserSupport@single');
        $router->post('/user/support/compose', 'Investor\UserSupport@composeOrReply');
    });

});


$router->group(['guest' => 'root'], function() use($router){
    $router->get('/checking', 'Guest\LoginController@checking');
    $router->get('/checkingTran/{transID}', 'Guest\LoginController@checkingTran');
    $router->get('/user/investment/invoice/{investID}/{userID}', 'Investor\HtmlToPdf@createPDF');
    // $router->get('/user/investment/generateInvoice/{investID}', 'Investor\InvestmentManager@generateInvoice');
    // $router->get('/home', 'guest\LoginController@home');
    $router->post('/login/authenticate', 'Guest\LoginController@sendLoginToken');
    $router->post('/login', 'Guest\LoginController@loginHere');
    $router->post('/register', 'Guest\RegisterController@createRecord');
    $router->post('/forgot-password', 'Guest\ForgotPassword@verifyEmail');
    $router->post('/forgot-password/submit-token', 'Guest\ForgotPassword@verifyToken');
    $router->post('/forgot-password/reset-password', 'Guest\ForgotPassword@resetPassword');
    $router->post('/fileupload/{folder}/{name}', 'Guest\FileManager@uploadProcess');

    $router->group(['contents' => 'root'], function($router){
        $router->get('/contents', 'Guest\ContentManager@homeContent');
        $router->get('/contents/faq', 'Guest\ContentManager@faq');
        $router->get('/general_settings', 'Guest\GeneralSettings@index');
        $router->get('/contents/banks', 'Guest\ContentManager@banks');
        $router->get('/contents/pages/{url}', 'Guest\ContentManager@pages');
        $router->get('/investments', 'Guest\Investment@plan');
        $router->get('/investments/digest', 'Guest\Investment@digest');
        $router->get('/testimony', 'Guest\TestimonyGuest@getTestimony');
        $router->post('/contact', 'Guest\Contact@index');
        $router->get('/currency', 'Guest\GeneralSettings@defaultCurrency');
    });
    $router->get('/testcoin', 'CoinPay\Examples\createSimpleTransaction@justTest');
    $router->get('/justCovert', 'CoinPay\Examples\ConvertCoin@convertPay');
    $router->get('/fiatPrice', 'CoinPay\Examples\FiatToCoinPrices@fiatPrice');
    $router->get('/convertLimit', 'CoinPay\Examples\ConversionLimit@convertLimit');


    //coinremitter success URL
    $router->get('/coinremitter_success/{ref}', 'Guest\CoinRemitterResp@successResp');
    $router->get('/coinremitter_failed/{ref}', 'Guest\CoinRemitterResp@failedResp');
});



$router->group(['admin' => 'root'], function() use($router){
    $router->post('/admin/login', 'Administrator\AdminLogin@index');
    $router->get('/admin/logout/{token}', 'Administrator\AdminLogin@logout');
    // $router->get('/admin/withdrawals/{limit}/{page}', 'Administrator\WithdrawalManager@loginHere');
    
    // $router->group(['middleware' => 'auth'], function() use($router){
        $router->get('/admin/dashboard/{token}', 'Administrator\DashboardInfo@index');
        
        $router->get('/admin/withdrawals/{token}/{status}/{limit}/{page}', 'Administrator\WithdrawalManager@withdrawals');
        $router->get('/admin/withdrawal/approve/{token}/{id}', 'Administrator\WithdrawalManager@approveWithdrawal');
        $router->delete('/admin/withdrawal/delete/{token}/{id}', 'Administrator\WithdrawalManager@deleteWithdrawal');


        // WALLET FUNDINGS
        $router->get('/admin/wallet-fund/{token}/{status}/{limit}/{page}', 'Administrator\WalletManager@WalletFund');
        $router->get('/admin/wallet-fund/approve/{token}/{id}', 'Administrator\WalletManager@approveWalletFund');
        $router->delete('/admin/wallet-fund/delete/{token}/{id}', 'Administrator\WalletManager@deleteWalletFund');
        
        //INVESTOR MANAGER
        $router->get('/admin/investors/{token}/{role}/{limit}/{page}', 'Administrator\InvestorManager@investors');
        $router->get('/admin/investor/single/{token}/{userID}', 'Administrator\InvestorManager@investorDetails');
        $router->post('/admin/investor/update/{token}', 'Administrator\InvestorManager@updateInvestor');
        $router->delete('/admin/investor/delete/{token}/{user_id}', 'Administrator\InvestorManager@deleteInvestor');
        $router->put('/admin/investor/update-bank/{token}', 'Administrator\InvestorManager@updateUserBank');
        $router->delete('/admin/investor/bank/delete/{token}/{bankID}', 'Administrator\InvestorManager@deleteBank');
        $router->get('/admin/investor/wallet/{token}/{userID}/{limit}/{page}', 'Administrator\InvestorManager@investorWallet');
        $router->get('/admin/investor/withdrawal/{token}/{userID}/{limit}/{page}', 'Administrator\InvestorManager@investorWithdrawal');
        $router->get('/admin/investor/account-action/{token}/{userID}/{action}', 'Administrator\InvestorManager@investorAction');
        $router->get('/admin/investor/referral/{token}/{userID}/{limit}/{page}', 'Administrator\InvestorManager@investorDownlines');
        $router->post('/admin/investor/wallet-action/{token}', 'Administrator\InvestorManager@userWalletAction');
        $router->get('/admin/investor/wallet-fundind/{token}/{userID}/{limit}/{page}', 'Administrator\InvestorManager@investorWalletFunding');
        
        
        $router->get('/admin/investor/search/{token}/{keyword}', 'Administrator\InvestorManager@searchInvestor');
        $router->post('/admin/investor/add-investor/{token}', 'Administrator\InvestorManager@addNewUser');
        $router->get('/admin/investor/verify-user/{token}/{userID}', 'Administrator\InvestorManager@verifyUser');
        
        // ADMIN MANAGER
        $router->group(['admin-manager' => 'administrator'], function($router){
            $router->get('/admin/admin-manager/all/{token}/{limit}/{page}', 'Administrator\AdminManager@admins');
            $router->post('/admin/admin-manager/add/{token}', 'Administrator\AdminManager@addAdmin');
            $router->get('/admin/admin-manager/single/{token}/{id}', 'Administrator\AdminManager@adminSingle');
            $router->put('/admin/admin-manager/update/{token}/{role}', 'Administrator\AdminManager@updateAdmin');
            $router->delete('/admin/admin-manager/delete/{token}/{id}', 'Administrator\AdminManager@deleteAdmin');
            $router->post('/admin/admin-manager/change-password/{token}', 'Administrator\AdminManager@changePassword');
        });

        // INVESTMENT MANAGER
        $router->get('/admin/investment/plan/{token}/{limit}/{page}/{status}', 'Administrator\InvestmentManager@plan');
        $router->post('/admin/investment/add-plan/{token}', 'Administrator\InvestmentManager@addPlan');
        $router->get('/admin/investment/plan/{token}/{id}', 'Administrator\InvestmentManager@singlePlan');
        $router->put('/admin/investment/update-plan/{token}', 'Administrator\InvestmentManager@updatePlan');
        $router->delete('/admin/investment/plan/delete/{token}/{id}', 'Administrator\InvestmentManager@deletePlan');
        
        $router->get('/admin/investment/{token}/{limit}/{page}/{status}', 'Administrator\InvestmentManager@investment');
        $router->get('/admin/investment/user/{token}/{loginID}', 'Administrator\InvestmentManager@investmentByUser');
        $router->get('/admin/investment/approve/{token}/{investID}', 'Administrator\InvestmentManager@approveInvest');
        $router->get('/admin/investment/history/{token}/{investID}', 'Administrator\InvestmentManager@investmentHistory');
        $router->post('/admin/investment/delete/{token}', 'Administrator\InvestmentManager@deleteInvestment');

        $router->get('/admin/investment/digest/{token}', 'Administrator\InvestmentManager@digest');

        $router->put('/admin/investment/digest/update/{token}', 'Administrator\InvestmentManager@updateDigest');
        

        // GENERAL SETTINGS
        $router->group(['general-settings' => 'administrator'], function($router){
            $router->put('/admin/update-web-settings/{token}', 'Administrator\GeneralSettings@updateWebsiteSettings');
            $router->put('/admin/update-configuration/{token}', 'Administrator\GeneralSettings@updateConfiguration');
            $router->post('/admin/add-social-link/{token}', 'Administrator\GeneralSettings@addSocialLink');
            $router->delete('/admin/delete-social-link/{token}/{id}', 'Administrator\GeneralSettings@deleteSocialLink');
            
            $router->get('/admin/currency/{token}', 'Administrator\GeneralSettings@getCurrency');
            $router->post('/admin/currency/add/{token}', 'Administrator\GeneralSettings@addCurrency');
            $router->get('/admin/currency/set-default/{token}/{id}', 'Administrator\GeneralSettings@setDefaultCurrency');
            $router->delete('/admin/currency/delete/{token}/{id}', 'Administrator\GeneralSettings@deleteCurrency');
        
            $router->put('/admin/update-payment-method/{token}', 'Administrator\GeneralSettings@updatePaymentMethod');
            $router->get('/admin/payment-method/{token}', 'Administrator\GeneralSettings@getPayMethAdmin');
        
            
            //BANK LIST
            $router->post('/admin/add-bank-list/{token}', 'Administrator\GeneralSettings@addBankList');
            $router->delete('/admin/delete-bank-list/{token}/{bankID}', 'Administrator\GeneralSettings@deleteBankList');
        });
    
        // Content Management
        $router->group(['content-management' => 'administrator'], function($router){
            $router->put('/admin/update-about-us/{token}', 'Administrator\ContentManager@updateAboutUs');
            
            $router->get('/admin/who-we-call-to/{token}', 'Administrator\ContentManager@getWhoWeAre');
            $router->put('/admin/update-who-we-are/{token}', 'Administrator\ContentManager@updateWhoWeAre');
            $router->put('/admin/update-call-to-action/{token}', 'Administrator\ContentManager@updateCallToAction');
            
            $router->get('/admin/how-it-work/{token}', 'Administrator\ContentManager@howItWorkList');
            $router->post('/admin/how-it-work/add/{token}', 'Administrator\ContentManager@addHowItWork');
            // $router->get('/admin/how-it-work/{id}', 'Administrator\ContentManager@howItWorkSingle');
            $router->put('/admin/how-it-work/update/{token}', 'Administrator\ContentManager@updateHowItWork');
            $router->delete('/admin/how-it-work/delete/{token}/{id}', 'Administrator\ContentManager@deleteHowItWork');

            $router->get('/admin/faq/{token}', 'Administrator\ContentManager@faqList');
            $router->post('/admin/faq/add/{token}', 'Administrator\ContentManager@addFaq');
            $router->put('/admin/faq/update/{token}', 'Administrator\ContentManager@updateFaq');
            $router->delete('/admin/faq/delete/{token}/{id}', 'Administrator\ContentManager@deleteFaq');
            
            $router->get('/admin/bank/{token}', 'Administrator\ContentManager@banks');
            $router->post('/admin/bank/add/{token}', 'Administrator\ContentManager@addBank');
            // $router->get('/admin/bank/{id}', 'Administrator\ContentManager@bankSingle');
            $router->put('/admin/bank/update/{token}', 'Administrator\ContentManager@updateBank');
            $router->delete('/admin/bank/delete/{token}/{id}', 'Administrator\ContentManager@deleteBank');
    
            $router->get('/admin/home-banner/{token}', 'Administrator\ContentManager@getHomeBanner');
            $router->put('/admin/home-banner/update/{token}', 'Administrator\ContentManager@updateHomeBanner');

            // OUR CLIENTS
            $router->get('/admin/client/{token}', 'Administrator\ContentManager@getClient');
            $router->post('/admin/client/add/{token}', 'Administrator\ContentManager@addClient');
            $router->delete('/admin/client/delete/{token}/{id}', 'Administrator\ContentManager@deleteClient');
        });
        
        // Pages
        $router->group(['pages' => 'administrator'], function($router){
            $router->get('/admin/page/{token}', 'Administrator\PagesManager@pages');
            $router->post('/admin/page/add/{token}', 'Administrator\PagesManager@addPage');
            $router->get('/admin/page/{token}/{id}', 'Administrator\PagesManager@pageSingle');
            $router->put('/admin/page/update/{token}', 'Administrator\PagesManager@updatePage');
            $router->delete('/admin/page/delete/{token}/{id}', 'Administrator\PagesManager@deletePage');
        });
        
        // NEWS MANAGER
        $router->group(['news' => 'administrator'], function($router){
            $router->get('/admin/news/{token}/{limit}/{page}', 'Administrator\NewsManager@index');
            $router->post('/admin/news/add/{token}', 'Administrator\NewsManager@addNews');
            $router->get('/admin/news/single/edit/{token}/{id}', 'Administrator\NewsManager@singleNews');
            $router->put('/admin/news/update/{token}', 'Administrator\NewsManager@updateNews');
            $router->delete('/admin/news/delete/{token}/{id}', 'Administrator\NewsManager@deleteNews');
        });

        // TESTIMONY MANAGER
        $router->group(['testimony' => 'administrator'], function($router){
            $router->get('/admin/testimonial/{token}/{limit}/{page}', 'Administrator\TestimonyManager@index');
            $router->post('/admin/testimonial/{token}/add', 'Administrator\TestimonyManager@addNews');
            $router->get('/admin/testimonial/single/edit/{token}/{id}', 'Administrator\TestimonyManager@singleTestimony');
            $router->put('/admin/testimonial/update/{token}', 'Administrator\TestimonyManager@updateTestimony');
            $router->delete('/admin/testimonial/delete/{token}/{id}', 'Administrator\TestimonyManager@deleteTestimony');
        });
        


        // EMAIL TEMPLATE
        $router->group(['testimony' => 'administrator'], function($router){
            $router->get('/admin/email-template/{token}', 'Administrator\ContentManager@emailTemplate');
            $router->get('/admin/email-template/single/edit/{token}/{id}', 'Administrator\ContentManager@singleEmailTemplate');
            $router->put('/admin/email-template/update/{token}', 'Administrator\ContentManager@updateEmailTemplate');
        });

        // ADMIN SUPPORT
        $router->group(['administrator' => 'support'], function() use($router){
            $router->get('/admin/support/{token}/{status}/{limit}/{page}', 'Administrator\SupportManager@index');
            $router->get('/admin/support/single/{token}/{id}', 'Administrator\SupportManager@single');
            $router->post('/admin/support/compose/{token}', 'Administrator\SupportManager@composeOrReply');
            $router->delete('/admin/support/delete/{token}/{id}', 'Administrator\SupportManager@deleteSupport');
        });

    // });
});